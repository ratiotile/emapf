#pragma once
#include <vector>

namespace EvolutionaryAlgorithms
{
class Chromosome
{
public:
	Chromosome();
	Chromosome(std::vector<float>& gene){Gene = gene;}
	
	/// Mutate a randomly number of the genes/values in the current Chromosome with a 
	/// random number drawn from a Gaussian distribution. with a mean and standard deviation
	void MutateGaussianRandom(float mean, float deviation);

	/**
	 * Mutate all the genes/values in the current Chromosome with a random number drawn from a Gaussian distribution. with a mean and standard deviation
	 * @param float mean
	 * @param float deviation - standard deviation
	 */
	void MutateGaussian(float mean, float deviation);

	float FlipFloatRandom(float aFloat);
	
	void FlipRandomAllWeights();

	void SetGene(std::vector<float> genesFloat);

	std::vector<float> Gene;
	int Fitness;
};
}