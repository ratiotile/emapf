#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_SCV_Agent : public UnitAgent
{
public:
	Terran_SCV_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_SCV_Agent";
		//SightRange = 7; 
	}
};
typedef boost::shared_ptr<Terran_SCV_Agent> pTerran_SCV_Agent;