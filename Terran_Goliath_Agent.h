#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_Goliath_Agent : public UnitAgent
{
public:
	Terran_Goliath_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_Goliath_Agent";
		//SightRange = 7; 
	}
};
typedef boost::shared_ptr<Terran_Goliath_Agent> pTerran_Goliath_Agent;