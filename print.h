#pragma once
#ifndef INCLUDED_PRINT_H
#define INCLUDED_PRINT_H
#include <sstream>
#include <iostream> // maybe needed for ofstream
#include <fstream> // logging
#include <BWAPI.h>


using namespace std;

extern ofstream logfile; // logging
// print vector, template class
template <typename T> void printArray( const T& array, const unsigned int n ){
	std::ostringstream oss;
	for (unsigned int i = 0; i < n; i++)
	{
		oss << array[i];
		if (i + 1 < n) oss << ", ";
	}
	Broodwar->printf("%s\n", oss.str().c_str());
}
template <typename T> void printLogArray( const T& array, const unsigned int n ){
	std::ostringstream oss;
	for (unsigned int i = 0; i < n; i++)
	{
		oss << array[i];
		if (i + 1 < n) oss << ", ";
	}
	logfile << oss.str() << endl;
}
// print 2d vector, template class
template <typename T> void print2DArray( const T& array, const unsigned int n ){
	for (unsigned int i = 0; i < n; i++)
	{
		printArray(array[i], n);
	}
	Broodwar->printf("\n");
}
template <typename T> void printLog2DArray( const T& array, const unsigned int n ){
	for (unsigned int i = 0; i < n; i++)
	{
		printLogArray(array[i], n);
	}
	logfile << endl;
}
template <typename T> void printLog2DArray( const T& array, const unsigned int n1, const unsigned int n2 ){
	std::ostringstream oss;
	for (unsigned int j = 0; j < n2; j++)
	{
		for (unsigned int i = 0; i < n1; i++)
		{
			oss << array[i][j];
			if (i + 1 < n1) oss << ", ";
		}
		oss << endl;
	}
	oss << endl;
	logfile << oss.str();
	logWriteBuffer();
}
/// force write buffer to file by closing/reopening
void logWriteBuffer();
/// clears log file
void logClear();
#endif