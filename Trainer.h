#pragma once
#include "Interface.h"
#include "EvolutionaryAlgorithm.h"
#include "Singleton.h"

using namespace EvolutionaryAlgorithms;
class Trainer: public Singleton<Trainer>
{
private:
	 string _unitAgentSerializeFileName;
	 int _numberOfRepopulations;
	 int _totalNumberOfRepopulations;
	 int _populationSize;
	 int _numberOfTrials;
	EvolutionaryAlgorithm* _ea;
	 int _gameSpeed;
	int saveCount;
	int _nuberOfCrossOvers;
	bool _saveXMLInSameFile;
	int _startGenerationOn;
	int _percentOfOpPropsToLoad;
	// ignore all others besides this value
	bool isInitialized;

	void reconnect();
public:
	// Default constructor , call setup inside
	Trainer();


	/// Create a complete new population with totally random values for all chromosomes.
	void SetupNewPopulation(bool tTest = false);

	void setup_newRandomPopulation();

	/// Create a population of existing chromosomes from a file with a possible 
	/// mixture of new random chromosomes.
	void SetupResumeTraining();

	/// Use the best optimized agent to play StarCraft Broodwar (using the 
	/// fittest unit agent optimized properties loaded from an XML file).
	void LoadBestAndPlay();

	/// Use the best optimized agents to play StarCraft Broodwar (using the 
	/// fittest unit agent optimized properties loaded from an XML file).
	void SetupTrainer(int numberOfRestarts, int populationSize, int gameSpeed);

	/// TEST METHOD for running with predefined unit agent optimized properties.
	static UnitAgentOptimizedProperties GetOptimizedValuesToUnitAgent(
		string unitTypeName);//ref UnitAgent unitAgent);

	void PlayWithoutTraining(int gameSpeed, int numberOfRestarts, bool loadAgent);

	void TTest();

	/// Train the population of candidates.
	void PlayWithRandomChromosomes();

	/// Train the population of candidates.
	void Train();
	void run();

	void Play(vector<UnitAgentOptimizedProperties>& optimizedPropertiesGeneList, 
		int setGameSpeed, bool setCompleteMapInfo, bool setUserInput);

	vector<UnitAgentOptimizedProperties> optimizedPropertiesGeneList;
	int gameSpeed;
	bool complemapinformation;
	bool userInput;
};