#pragma once
#include <string>
#include "Interface.h"
#include "Settings.h"
// XMLFileHandler.cs
#include "UnitAgentOptimizedProperties.h"

class OptimalPopertiesSerializer
{
public:

	OptimalPopertiesSerializer()
	{
		Filename = Settings::get().SerializeName;
	}
	OptimalPopertiesSerializer(std::string fileName)
	{
		Filename = fileName;
	}

	void serialize( const vector<UnitAgentOptimizedProperties>& data );
	vector<UnitAgentOptimizedProperties> deserialize();
	vector<UnitAgentOptimizedProperties> deserialize( const string& filename);
	// taken from BWTA
	bool fileExists(std::string filename);

	std::string Filename;
};
