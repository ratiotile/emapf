#pragma once
#include <BWAPI.h>
#include <boost/shared_ptr.hpp>
#include "print.h"

typedef BWAPI::TilePosition TilePosition;
typedef BWAPI::Position Position;
using BWAPI::Player;
using BWAPI::Broodwar;
using BWAPI::Unit;
using BWAPI::UnitType;

class UnitAgent;
typedef boost::shared_ptr<UnitAgent> pUnitAgent;

#include "WalkTile.h"
//#include "Unit.h"

using std::string;
using std::vector;
using std::set;
