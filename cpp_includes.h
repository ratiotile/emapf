#pragma once
#ifndef INCLUDED_CPP_INCLUDES_H
#define INCLUDED_CPP_INCLUDES_H

#include <assert.h>
#ifndef INCLUDED_LOGGING
	#define INCLUDED_LOGGING
	#include <fstream>
#endif
#ifndef INCLUDED_PRINT_H
	#include "print.h"
#endif
#include <boost/foreach.hpp>
#include "BWAPIext.h"
#include "Logger.h"

using namespace std;
using BWAPI::UnitType;
using BWAPI::Position;
using BWAPI::TilePosition;
using BWAPI::Broodwar;

using BWAPIext::WalkTile;
//using namespace BBS;
using namespace BWAPI::UnitTypes;
using namespace BWAPI::Races;

// logger wrapper
boost::log::sources::severity_logger<logging::trivial::severity_level>& lg();
using namespace logging::trivial;

#endif

