#include "UnitAgent.h"
#include "cpp_includes.h "
#include "PotentialFieldManager.h"
#include "MyMath.h"
#include "Polygon.h"
#include "Settings.h"
#include "DrawBuffer.h"
#include <sstream>
#include <limits>

using BWAPI::Unit;

UnitAgent::UnitAgent( Unit* myUnit, UnitAgentOptimizedProperties& opProp )
:MyUnit(myUnit), OptimizedProperties(opProp), LeadingStatus(LeadingStatusEnum::None)
,EmotionalMode(EmotionalModeEnum::None), HealthLevelOk(60), UnitAgentTypeName("Unit_Agent")
{
}

UnitAgent::UnitAgent()
:MyUnit(NULL)
{

}
void UnitAgent::ExecuteBestActionForUnitAgent( std::vector<pUnitAgent>& squad )
{
	if(MyUnit->isSelected() && Settings::get().displaySelectedPF)
	{
		int unitsizepx = MyUnit->getType().dimensionRight();
		int range = MyUnit->getType().groundWeapon().maxRange();
		if(GoalUnitToAttack == NULL || MyUnit->getDistance(GoalUnitToAttack) <= range )
		{
			DrawBuffer::get().drawBufferedCircle(BWAPI::CoordinateType::Map, MyUnit->getPosition().x(),
				MyUnit->getPosition().y(), unitsizepx+range ,Settings::get().PFUpdateInterval,BWAPI::Colors::Green);
		}
		else
		{
			DrawBuffer::get().drawBufferedCircle(BWAPI::CoordinateType::Map, MyUnit->getPosition().x(),
				MyUnit->getPosition().y(), unitsizepx+range ,Settings::get().PFUpdateInterval,BWAPI::Colors::Red);
		}
		if (Settings::get().userSelectedControl) return;
	}

	// player control debug.
	// don't interrupt unit when it is shooting
	if(MyUnit->isAttackFrame()) return;
	if (MyUnit->isVisible() && GoalUnitToAttack != NULL) //Always remember to check if the unit is visible, before Moving it.
	{
		SubGoalPosition = CalculateNewSubGoalPosition(squad, GoalUnitToAttack);

		if (MyUnit->getGroundWeaponCooldown() != 0)
		{
			MyUnit->move(SubGoalPosition);
		}
		else
		{
			//if (MyUnit->isInWeaponRange(GoalUnitToAttack))
			//	MyUnit->attack(GoalUnitToAttack);
			//else
				MyUnit->attack(SubGoalPosition);
		}
	}
}

BWAPI::Position UnitAgent::CalculateNewSubGoalPosition( std::vector<pUnitAgent>& squad, BWAPI::Unit* closestEnemyUnit )
{
	int currentX = MyUnit->getPosition().x();
	int currentY = MyUnit->getPosition().y();
	vector<Position> surroundingPositions = 
		PotentialFieldManager::get().getPotentialFieldPositions(MyUnit);

	double currentPotentialFieldValue = -1 * numeric_limits<double>::max();
	double bestPotentialFieldValue = -1 * numeric_limits<double>::max();
	Position bestPosition = MyUnit->getPosition();

	for each (Position possibleNextPosition in surroundingPositions)
	{
		currentPotentialFieldValue = CalculatePotentialField(squad, closestEnemyUnit, possibleNextPosition);
		if (currentPotentialFieldValue > bestPotentialFieldValue)
		{
			bestPotentialFieldValue = currentPotentialFieldValue;
			bestPosition = possibleNextPosition;
		}
		// display debug
		if (Settings::get().displaySelectedPF && MyUnit->isSelected()){
			std::stringstream ss;
			ss << (int)bestPotentialFieldValue;
			DrawBuffer::get().drawBufferedText(BWAPI::CoordinateType::Map,
				possibleNextPosition.x(), possibleNextPosition.y(),ss.str(),
				Settings::get().PFUpdateInterval);
		}
	}
	if (Settings::get().displaySelectedPF && MyUnit->isSelected()){
		DrawBuffer::get().drawBufferedBox(BWAPI::CoordinateType::Map,
			bestPosition.x()-16,bestPosition.y()-16,bestPosition.x()+16,bestPosition.y()+16,
			Settings::get().PFUpdateInterval);
	}
	return bestPosition;
}

double UnitAgent::CalculatePotentialField( std::vector<pUnitAgent>& squad, BWAPI::Unit* enemy, Position field )
{
	double pVal = 0;

	//DO NOT UPDATE WITH EVOLUTIONARY ALGORITHM
	const double forceNeutralUnitsRepulsion = 200; // 0 - 1000 //REMEMBER TO SET TO A POSITIVE NUMBER.
	const double forceStepNeutralUnitsRepulsion = 1.2; // 0 - 10
	int rangeNeutralUnitsRepulsion = 8; //0-512
	// ///////////////////////////////////////////

	double distance = enemy->getPosition().getDistance(field); 

	//If the weapon is ready to fire, PFMaxShootingDistanceAttraction is activated
	if (MyUnit->getGroundWeaponCooldown() == 0) 
		pVal += PFMaxShootingDistanceAttraction(distance, enemy, OptimizedProperties.ForceMSD, 
		OptimizedProperties.ForceStepMSD);//, MSDDiffDivValue);

	//Else If Weapon is ready to fire and AttackRange is bigger than 1, 
	// FLEE/RETREAT until weapon is ready to fire again.
	else if (MyUnit->getType().seekRange() > 1) 
		pVal += PFWeaponCoolDownRepulsion(field, distance, enemy, OptimizedProperties.ForceWeaponCoolDownRepulsion, 
		OptimizedProperties.ForceStepWeaponCoolDownRepulsion, OptimizedProperties.RangeWeaponCooldownRepulsion);

	//Squad attraction
	if (squad.size()>2)
		pVal += PFSquadAttraction(squad, field, OptimizedProperties.ForceSquadAttraction, OptimizedProperties.ForceStepSquadAttraction, OptimizedProperties.RangePercentageSquadAttraction);

	//Center of the map attraction (NEGATES the MSD)
	pVal += PFMapCenterAttraction(field, OptimizedProperties.ForceMapCenterAttraction, OptimizedProperties.ForceStepMapCenterAttraction,
		OptimizedProperties.RangePecentageMapCenterAttraction);

	//Map edge repulsion (NEGATES the MSD)
	pVal += MyMath::CalculateMapEdgeRepulsion(field, OptimizedProperties.ForceMapEdgeRepulsion, OptimizedProperties.ForceStepMapEdgeRepulsion, OptimizedProperties.RangeMapEdgeRepulsion);

	//Own Unit Repulsion
	pVal += PFOwnUnitsRepulsion(squad, field, OptimizedProperties.ForceOwnUnitsRepulsion,
		OptimizedProperties.ForceStepOwnUnitsRepulsion, OptimizedProperties.RangeOwnUnitsRepulsion);

	//Enemy Unit Repulsion
	pVal += PFEnemyUnitsRepulsion(field, OptimizedProperties.ForceEnemyUnitsRepulsion,
		OptimizedProperties.ForceStepEnemyUnitsRepulsion);//, rangeEnemyUnitsRepulsion);

	//Neutral Unit Repulsion
	pVal += PFNeutralUnitsRepulsion(field, forceNeutralUnitsRepulsion, 
		forceStepNeutralUnitsRepulsion, rangeNeutralUnitsRepulsion);
	return pVal;
}

double UnitAgent::PFMaxShootingDistanceAttraction( double distanceToEnemy, BWAPI::Unit* enemyUnit, 
												  double forceToOwnMSD, double forceStepToOwnMSD )
{
	int enemyr = enemyUnit->getType().dimensionRight();
	int myr = MyUnit->getType().dimensionRight();
	distanceToEnemy -= (enemyr+myr);
	
	double ownUnitMSD = MyUnit->getType().groundWeapon().maxRange(); // / TileSize;// 
	double enemyUnitMSD = enemyUnit->getType().groundWeapon().maxRange();
	double MSDDifference = ownUnitMSD - enemyUnitMSD;

	double potenTialValueMSD = 0; //Weight potenTialValueMSD is used to alter the relative strength of the current tile, which the unit stands on or the possible tiles the unit can go to.

	if (distanceToEnemy > ownUnitMSD) //GET CLOSER TO ENEMY
		return forceToOwnMSD - forceStepToOwnMSD * (distanceToEnemy - ownUnitMSD);

	if (MSDDifference > 0 && distanceToEnemy <= ownUnitMSD) // good. shoot, then back off
	{
		if (distanceToEnemy > enemyUnitMSD)
			return forceToOwnMSD + forceStepToOwnMSD * (distanceToEnemy - enemyUnitMSD);
		else // too close
			return distanceToEnemy;
	}
	else if (distanceToEnemy <= ownUnitMSD) //TOO CLOSE TO ENEMY
		return forceToOwnMSD;
	return potenTialValueMSD;
}

double UnitAgent::PFOwnUnitsRepulsion( std::vector<pUnitAgent>& squad, Position field, double force, 
									  double forceStep, int range )
{
	double pfValue = 0;

	for each (pUnitAgent myOtherUnit in squad)
	{
		pfValue = MyMath::CalculatePFOwnUnitRepulsion(MyUnit, field, myOtherUnit->MyUnit, force, 
			forceStep, range);
		if (pfValue < 0)
			return pfValue;
	}

	return 0;
}

double UnitAgent::PFWeaponCoolDownRepulsion( Position field, double distanceToEnemy, 
							Unit* enemy, double force, double forceStep, int range )
{
	if (enemy->isVisible() && IsInEnemyUnitsRange(field, range)) 
		return PotentialFieldManager::get().CalculateRepulsiveMagnitude(force, forceStep, 
			distanceToEnemy, true);
	return 0;
}

bool UnitAgent::IsInEnemyUnitsRange( Position field, int extraRange )
{

	for each (Unit* enemyUnit in Broodwar->enemy()->getUnits())
	{
		if (CanBeAttacked(MyUnit, enemyUnit)){
			if (enemyUnit->getDistance(field) <= enemyUnit->getType().seekRange() + extraRange){
				return true;
			}
		}
	}

	return false;
}

bool UnitAgent::CanBeAttacked( Unit* ownUnit, Unit* enemyUnit )
{
	UnitType ownUnitType = ownUnit->getType();
	UnitType enemyUnitType = enemyUnit->getType();

	if (ownUnitType.isFlyer())
	{
		//Can enemy unit attack air units.
		if (enemyUnitType.groundWeapon().targetsAir())
			return true;
		if (enemyUnitType.airWeapon().targetsAir())
			return true;
	}
	else
	{
		//Can enemy unit attack ground units.
		if (enemyUnitType.groundWeapon().targetsGround())
			return true;
		if (enemyUnitType.airWeapon().targetsGround())
			return true;
	}
	return false;
}

double UnitAgent::PFSquadAttraction( std::vector<pUnitAgent>& squad, Position possibleNextPosition, 
									double force, double forceStep, int rangePercentage )
{
	vector<Position> unitPositions;
	unitPositions.reserve(squad.size());

	if (Broodwar->self()->getUnits().size() > 0)
	{
		int i = 0;
		for each (pUnitAgent meleeUnitAgent in squad)
		{
			unitPositions.push_back(meleeUnitAgent->MyUnit->getPosition());
		}

		Polygon meleeUnitPolygon(unitPositions);
		double distance = possibleNextPosition.getDistance(meleeUnitPolygon.FindCentroid());
		return distance <= MyMath::GetPercentageOfMaxDistancePixels(rangePercentage)
			? force//0
			: force - forceStep * distance;
	}
	logfile << "Enemy unit list is null or has zero elements in CalculatePFValueForCollisionWithEnemyUnits";
	return 0;
}

double UnitAgent::PFMapCenterAttraction( Position field, double force, double forceStep, int rangePercentage )
{
	double distance = field.getDistance(MyMath::GetMapCenterPosition());
	return distance <= MyMath::GetPercentageOfMaxDistancePixels(rangePercentage)
		? force//0
		: force - forceStep * distance;
}

double UnitAgent::PFEnemyUnitsRepulsion( Position field, double force, double forceStep )
{
	double pfValue = 0;
	//for each (Unit myOtherUnit in unitsInMyArea)

	for each (Unit* enemyUnit in Broodwar->enemy()->getUnits())
	{
		pfValue = MyMath::CalculatePFEnemyUnitRepulsion(MyUnit, field, enemyUnit, force, forceStep);
		if (pfValue < 0)
			return pfValue;
	}

	return 0;
}

double UnitAgent::PFNeutralUnitsRepulsion( Position field, double force, double forceStep, int range )
{
	double pfValue = 0;

	for each (Unit* neutralUnit in Broodwar->getNeutralUnits())
	{
		pfValue = MyMath::CalculatePFNeutralUnitRepulsion(neutralUnit, field, force, forceStep, range);
		if (pfValue < 0)
			return pfValue;
	}

	return 0;
}