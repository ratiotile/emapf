#pragma once
#include "UnitAgentOptimizedProperties.h"
#include <BWAPI.h>
#include "Interface.h"
#include <boost/shared_ptr.hpp>


namespace EmotionalModeEnum
{
	enum EmotionalModeEnum
	{
		Defensive, Offensive, Exploration, Mining, Healing, Reparing, None
	};
};
namespace LeadingStatusEnum
{
	enum LeadingStatusEnum
	{
		GroupLeader, Follower, None
	};
}
class UnitAgent;
typedef boost::shared_ptr<UnitAgent> pUnitAgent;
class UnitAgent
{
public:
	

	UnitAgent();
	UnitAgent(BWAPI::Unit* myUnit, UnitAgentOptimizedProperties& opProp);

	void ExecuteBestActionForUnitAgent(std::vector<boost::shared_ptr<UnitAgent>>& squad);

	BWAPI::Position CalculateNewSubGoalPosition(std::vector<pUnitAgent>& squad, BWAPI::Unit* closestEnemyUnit);

	double CalculatePotentialField(std::vector<pUnitAgent>& squad, BWAPI::Unit* enemy, Position field);
	
	double PFMaxShootingDistanceAttraction(double distanceToEnemy, BWAPI::Unit* enemyUnit, 
		double forceToOwnMSD, double forceStepToOwnMSD);

	double PFOwnUnitsRepulsion(std::vector<pUnitAgent>& squad, Position field, double force, 
								double forceStep, int range);

	double PFWeaponCoolDownRepulsion(Position field, double distanceToEnemy, BWAPI::Unit* enemy, 
		double force, double forceStep, int range);

	double PFSquadAttraction(std::vector<pUnitAgent>& squad, Position possibleNextPosition, double force, 
		double forceStep, int rangePercentage);

	double PFMapCenterAttraction(Position field, double force, double forceStep, int rangePercentage);
	double PFEnemyUnitsRepulsion(Position field, double force, double forceStep);
	double PFNeutralUnitsRepulsion(Position field, double force, double forceStep, int range);;

	bool IsInEnemyUnitsRange(Position field, int extraRange);
	bool CanBeAttacked(Unit* ownUnit, Unit* enemyUnit);

	int LastHealth;

	UnitAgentOptimizedProperties OptimizedProperties;
	BWAPI::Position GoalPosition;
	BWAPI::Unit* GoalUnitToAttack;
	BWAPI::Position SubGoalPosition;
	BWAPI::Unit* MyUnit;
	std::string UnitAgentTypeName;
	EmotionalModeEnum::EmotionalModeEnum EmotionalMode;
	LeadingStatusEnum::LeadingStatusEnum LeadingStatus;
	int HealthLevelOk;

	//static const UnitAgent nullAgent;
};

