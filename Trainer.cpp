#include "Trainer.h"
#include "cpp_includes.h"
#include "Scoring.h"
#include <sstream>
#include <BWAPI/Client.h>
#include <stdio.h>
#include "BotEvents.h"

using namespace BWAPI;

Trainer::Trainer()
:	isInitialized(false),
gameSpeed(0), 
complemapinformation(true),
userInput(true)
{
	Settings& s = Settings::get();
	_totalNumberOfRepopulations = s.totalNumberOfRepopulations;
	_numberOfRepopulations = s.totalNumberOfRepopulations; // mod later if important
	_startGenerationOn = s.startGenerationOn; //The next generation after the one we have last time (can be used after a crash or if we just want to continue evolving from previous evolved data)
	_percentOfOpPropsToLoad = s.percentOfOpPropsToLoad; //How big a percentage of the unit agent optimized properties from the XML file should be loaded into a new generation.
	_populationSize = s.populationSize;//40;//80;//60;
	_numberOfTrials = s.numberOfTrials;
	_nuberOfCrossOvers = s.numberOfCrossovers;
	_gameSpeed = s.gameSpeed;//0; //0 is fastest. -1 Resets the speed to normal speed. 
	_unitAgentSerializeFileName = s.SerializeName;
	_saveXMLInSameFile = false; // default
	// _ea still not initialized
}

void Trainer::SetupNewPopulation(bool tTest)
{
	BOOST_LOG_SEV(lg(),info) << "setting up Trainer";
	saveCount = 1;
	_saveXMLInSameFile = false;
	_ea = new EvolutionaryAlgorithm(_populationSize);

	if (tTest){
		PlayWithRandomChromosomes();//TTest();
	}
	else{
		BOOST_LOG_SEV(lg(),info) << "Calling Train()";
		Train();
	}
}

void Trainer::setup_newRandomPopulation()
{
	saveCount = 1;
}

void Trainer::SetupResumeTraining()

{
	Settings& s = Settings::get();
	saveCount = 1;
	_saveXMLInSameFile = false;
	if (_totalNumberOfRepopulations > _startGenerationOn)
		_numberOfRepopulations = _totalNumberOfRepopulations - _startGenerationOn;
	else
	{
		logfile << "numberOfRepopulations needs to be bigger than startGenerationOn.\n";
		throw std::exception("numberOfRepopulations needs to be bigger than startGenerationOn.");
	}
	string loadFileName = s.loadFileName;
	_ea = new EvolutionaryAlgorithm(_populationSize, _percentOfOpPropsToLoad,loadFileName);
	assert(_ea->Population.size() > 0);
	Train();
}

void Trainer::LoadBestAndPlay()
{
	saveCount = 1;
	_saveXMLInSameFile = false;
	unsigned int numberOfRestarts = _numberOfRepopulations;
	string loadFileName = Settings::get().loadFileName;
	_ea = new EvolutionaryAlgorithm(_populationSize, loadFileName);

	PlayWithoutTraining(_gameSpeed, numberOfRestarts, true);
	//loadAgent: TRUE: CALL PLAY and use the current best optimized XMLfile, 
	//else use test method to load some standard values.
}

void Trainer::SetupTrainer( int numberOfRestarts, int populationSize, 
	int gameSpeed)
{
	saveCount = 1;
	_saveXMLInSameFile = false;
	_numberOfRepopulations = numberOfRestarts;
	_populationSize = populationSize;
	_unitAgentSerializeFileName = "";
	vector<string> FileNames;
	FileNames.push_back("Terran_Goliath.info");
	FileNames.push_back("Terran_Vulture.info");
	FileNames.push_back("Terran_SiegeTank.info");
	_ea = new EvolutionaryAlgorithm(_populationSize, FileNames);
	_gameSpeed = gameSpeed;

	

	PlayWithoutTraining(_gameSpeed, numberOfRestarts, true);
	//loadAgent: TRUE: CALL PLAY and use the current best optimized XMLfile, 
//else use test method to load some standard values.
}



void Trainer::run()
{
	string runOption = Settings::get().runOption;
	isInitialized = true;
	if(runOption == "TrainRandom") // train from start with random genes
	{
		BOOST_LOG_SEV(lg(),info) << "Training from random population.";
		SetupNewPopulation();
	}
	else if(runOption == "ResumeTrainFile")
	{
		BOOST_LOG_SEV(lg(),info) << "Resuming training from file.";
		SetupResumeTraining();
	}
	else if(runOption == "LoadBestAndPlay")
	{
		BOOST_LOG_SEV(lg(),info) << "Loading best individual from file and playing.";
		LoadBestAndPlay();
	}
	else
	{
		printf("invalid runOption!\n");
	}

}
UnitAgentOptimizedProperties Trainer::GetOptimizedValuesToUnitAgent( string unitTypeName ) /*ref UnitAgent unitAgent) */
{
	UnitAgentOptimizedProperties opProp;
	opProp.UnitTypeName = unitTypeName;

	//Force
	opProp.ForceOwnUnitsRepulsion = 100;
	opProp.ForceEnemyUnitsRepulsion = 200;
	opProp.ForceMSD = 240;
	opProp.ForceSquadAttraction = 10;
	opProp.ForceMapCenterAttraction = 20;
	opProp.ForceMapEdgeRepulsion = 50;
	opProp.ForceWeaponCoolDownRepulsion = 800;

	//ForceStep
	opProp.ForceStepOwnUnitsRepulsion = 0.6;
	opProp.ForceStepEnemyUnitsRepulsion = 1.2;
	opProp.ForceStepMSD = 0.24;
	opProp.ForceStepSquadAttraction = 0.1;
	opProp.ForceStepMapCenterAttraction = 0.09;
	opProp.ForceStepMapEdgeRepulsion = 0.3;
	opProp.ForceStepWeaponCoolDownRepulsion = 6.4;

	//Range
	opProp.RangeOwnUnitsRepulsion = 8;
	opProp.RangePercentageSquadAttraction = 5;
	opProp.RangePecentageMapCenterAttraction = 5;
	opProp.RangeMapEdgeRepulsion = 96;
	opProp.RangeWeaponCooldownRepulsion = 160;

	return opProp;
}

void Trainer::PlayWithoutTraining( int gameSpeed, int numberOfRestarts, bool loadAgent )
{
	BOOST_LOG_SEV(lg(),trace) << "waiting to connect";
	printf("Waiting for StarCraft Broodwar to be started from Chaoslauncher."); //Connecting...");
	reconnect();

	for (int i = 0; i < numberOfRestarts; i++)
	{                     
		BOOST_LOG_SEV(lg(),info) << "run number: " + i;
		BOOST_LOG_SEV(lg(),info) << "\nThe bot is getting ready for battle!\n";

		if (loadAgent){
			Play(_ea->OptimizedPropertiesGenes, gameSpeed, true, true);
		}
		else // TODO: figure this out
		{
			vector<UnitAgentOptimizedProperties> optProps;
			optProps.push_back(GetOptimizedValuesToUnitAgent("TESTUNIT"));
			Play(optProps,gameSpeed, true, true);
		}

		BOOST_LOG_SEV(lg(),info) << "Game ended";
		BOOST_LOG_SEV(lg(),info) << "Fitness Score After end game: " + Scoring::Fitness;
	}
}

void Trainer::TTest()
{
	try
	{
		//SWIG.BWAPI.bwapi.BWAPI_init();

		//printf("Waiting for StarCraft Broodwar to be started from Chaoslauncher."); //Connecting...");
		// 			Reconnect();
		// 
		// 			printf("The bot is now connected to StarCraft Broodwar");
		// 			printf("Waiting for the match to begin");

		// 			BOOST_LOG_SEV(lg(),info) << 
		// 				"c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11;c12;c13;c14;c15;c16;c17;c18;c19;c20;c21;c22;c23;c24;c25;c26;c27;c28;c29;c30;c31;c32;c33;c34;c35;c36;c37;c38;c39;c40;c41;c42;c43;c44;c45;c46;c47;c48;c49;c50;c51;c52;c53;c54;c55;c56;c57;c58;c59;c60;c61;c62;c63;c64;c65;c66;c67;c68;c69;c70;c71;c72;c73;c74;c75;c76;c77;c78;c79;c80;c81;c82;c83;c84;c85;c86;c87;c88;c89;c90;c91;c92;c93;c94;c95;c96;c97;c98;c99;c100");
		for (int i = 1; i <= _numberOfRepopulations; i++)
		{
			//printf("Round " + i);
			int indexOfOptimizedPropertiesGenes = 0;
			for(vector<UnitAgentChromosome>::iterator it = _ea->Population.begin();
				it != _ea->Population.end(); ++it)
			{
				UnitAgentChromosome& optimizedPropertiesChromosome = (*it);
				//UnitAgentOptimizedProperties optimizedPropertiesGene = _ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes];
				vector<UnitAgentOptimizedProperties> optimizedPropertiesGeneList;
				//=  () { _ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes] };
				optimizedPropertiesGeneList.push_back(_ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes]);
				Play(optimizedPropertiesGeneList, _gameSpeed, true, false); //Should take a parameter representing UnitAgentOptimizedProperties

				//Logger.Logger.AddAndPrintSingleLine("" + Scoring::Fitness + ";");

				//if ((indexOfOptimizedPropertiesGenes % 10) == 0)
				//printf(".");

				indexOfOptimizedPropertiesGenes++;
			}
			//BOOST_LOG_SEV(lg(),info) << "");
		}

		//printf("...SAVING FOR THE LAST TIME...");
		//_ea->SaveOptimizedPropertiesToXMLFile();
		//printf("...SAVING DONE...");
		//printf("...The EA has finished. Good bye for this time...");
	}
	catch (std::exception e)
	{
		// 			printf("Error: {0}", e);
		// 			printf(e.StackTrace);
		// 			AppDomain currentDomain = AppDomain.CurrentDomain;
		// 			currentDomain.UnhandledException += LastChanceHandler;
	}
}

void Trainer::PlayWithRandomChromosomes()
{
	try
	{
		// 			SWIG.BWAPI.bwapi.BWAPI_init();
		// 			printf("Waiting for StarCraft Broodwar to be started from Chaoslauncher."); //Connecting...");
		// 			Reconnect();
		int fitnessScore = 0;

		// 			printf("The bot is now connected to StarCraft Broodwar");
		// 			printf("Waiting for the match to begin");

		// 			Logger.Logger.AddAndPrintSingleLine(
		// 				"c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11;c12;c13;c14;c15;c16;c17;c18;c19;c20;c21;c22;c23;c24;c25;c26;c27;c28;c29;c30;c31;c32;c33;c34;c35;c36;c37;c38;c39;c40;c41;c42;c43;c44;c45;c46;c47;c48;c49;c50;c51;c52;c53;c54;c55;c56;c57;c58;c59;c60;c61;c62;c63;c64;c65;c66;c67;c68;c69;c70;c71;c72;c73;c74;c75;c76;c77;c78;c79;c80;c81;c82;c83;c84;c85;c86;c87;c88;c89;c90;c91;c92;c93;c94;c95;c96;c97;c98;c99;c100");

		for (int i = 1; i <= _numberOfRepopulations; i++)
		{
			// 				printf("Round " + i);
			// 				BOOST_LOG_SEV(lg(),info) << "");
			int indexOfOptimizedPropertiesGenes = 0;

			for(vector<UnitAgentChromosome>::iterator it = _ea->Population.begin();
				it != _ea->Population.end(); ++it)
			{
				UnitAgentChromosome& optimizedPropertiesChromosome = (*it);
				//UnitAgentOptimizedProperties optimizedPropertiesGene = _ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes];
				vector<UnitAgentOptimizedProperties> optimizedPropertiesGeneList;
				//= new () { _ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes] };

				//for (int trials = 0; trials < 2; trials++)
				//{
				Play(optimizedPropertiesGeneList, _gameSpeed, true, false); //Should take a parameter representing UnitAgentOptimizedProperties
				fitnessScore += Scoring::Fitness;
				// }
				optimizedPropertiesChromosome.Fitness = fitnessScore;
				fitnessScore = 0;

				// 					Logger.Logger.AddAndPrintSingleLine("" + optimizedPropertiesChromosome.Fitness + ";");
				// 					printf("Fitness score: " + optimizedPropertiesChromosome.Fitness);

				// 					if ((indexOfOptimizedPropertiesGenes % 10) == 0)
				// 						printf(".");

				indexOfOptimizedPropertiesGenes++;
			}
		}

		//printf("...The EA has finished. Good bye for this time...");
	}
	catch (std::exception e)
	{
		// 			printf("Error: {0}", e);
		// 			printf(e.StackTrace);
		// 			AppDomain currentDomain = AppDomain.CurrentDomain;
		// 			currentDomain.UnhandledException += LastChanceHandler;
	}
}

void Trainer::Train()
{
		// 			SWIG.BWAPI.bwapi.BWAPI_init();
		/// ALready Initialized!
		BOOST_LOG_SEV(lg(),trace) << "Train waiting to connect";
		printf("Waiting for StarCraft Broodwar to be started from Chaoslauncher."); //Connecting...");
		reconnect();
		int fitnessScore = 0;


		printf("The bot is now connected to StarCraft Broodwar\n");
		printf("Waiting for the match to begin\n");

		bool repopulate = false;

		if (_unitAgentSerializeFileName.empty()) //whut doues this do?
			BOOST_LOG_SEV(lg(),info) << "Best ; Worst ; Average";

		//Only repopulate from start in first generation, if data has been loaded from a file 
		// and fitness values exists. 
		else if ( _ea->OptimizedPropertiesGenes[0].Fitness != 0) 
		{
			repopulate = true;
		}

		for (int i = 0; i <= _numberOfRepopulations; i++)
		{
			int roundNr = i + _startGenerationOn;
			printf("Round %i\n", roundNr);

			if (repopulate)
				_ea->Repopulate(_nuberOfCrossOvers); 

			int indexOfOptimizedPropertiesGenes = 0;	
			// can't use for each here, it creates a copy!
			for(vector<UnitAgentChromosome>::iterator it = _ea->Population.begin();
				it != _ea->Population.end(); ++it)
			{
				UnitAgentChromosome& optimizedPropertiesChromosome = (*it);
				if (true)
				{
					//UnitAgentOptimizedProperties optimizedPropertiesGene = _ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes];
					vector<UnitAgentOptimizedProperties> optimizedPropertiesGeneList;
					optimizedPropertiesGeneList.push_back(_ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes]);
					// need to fill the new vector with _ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes]

					for (int trials = 0; trials < _numberOfTrials; trials++)
					{
						Play(optimizedPropertiesGeneList,_gameSpeed, true, true); //optimizedPropertiesGene, _gameSpeed, true, true, false); 
						// NOTE: play should update Fitness
						fitnessScore += Scoring::Fitness;
						
					}
					// normalize for number of trials
					fitnessScore = static_cast<int>(floor(0.5f + fitnessScore/(float)_numberOfTrials));
					optimizedPropertiesChromosome.Fitness = fitnessScore;
					printf("Fitness %i added to Chromosome\n",fitnessScore);
					//ONLY NECESSARY IF WE NEED TO SAVE THE FIRST RANDOM POPULATION WITH THEIR FITNESS SCORE 
					if (!_saveXMLInSameFile){//i == 0 && //AFTER FIRST ROUND THIS WILL HAPPEN AUTOMATICALLY, BECAUSE THE UnitAgentChromosome WILL BE CONVERTED TO UnitAgentOptimizedProperties in the repopulate medtod.
						_ea->OptimizedPropertiesGenes[indexOfOptimizedPropertiesGenes].Fitness = fitnessScore; }                    
					////
					fitnessScore = 0;

					if ((indexOfOptimizedPropertiesGenes % 10) == 0){
						printf(".");}

					indexOfOptimizedPropertiesGenes++;
				}
				else
				{
					BOOST_LOG_SEV(lg(),error) << "The _ea->OptimizedPropertiesGenes is null in Trainer.cs->Train().";
					throw std::exception("The _ea->OptimizedPropertiesGenes is null in Trainer.cs->Train().");
				}
			}



			if (!_saveXMLInSameFile)
			{
				if (!_ea->OptimizedPropertiesGenes.empty())
				{
					sort(_ea->OptimizedPropertiesGenes.begin(),_ea->OptimizedPropertiesGenes.end());
					int rank = 1;
					for(vector<UnitAgentOptimizedProperties>::iterator it = _ea->OptimizedPropertiesGenes.begin();
						it != _ea->OptimizedPropertiesGenes.end(); ++it)
					{
						it->Rank = rank;
						++rank;
					}
				}
				int genPrintNumber = i + _startGenerationOn;
				std::stringstream fname;
				fname << "!StarCraftUnitAgent" << genPrintNumber << ".txt";
				_ea->UnitAgentFileHandler.Filename = fname.str();
				printf("...SAVING...\n");
				_ea->SaveOptimizedPropertiesToXMLFile();
			  printf("...SAVING DONE...\n");
			}
			else if ((i % saveCount) == 0)//Save the best calculated agent after each n repopulation.
			{
				printf("...SAVING...\n");
				_ea->SaveOptimizedPropertiesToXMLFile();
				printf("...SAVING DONE...\n");
			}
			BOOST_LOG_SEV(lg(),info) << "Generation " << roundNr + 1;
			logfile << roundNr + 1 << ",";
			_ea->LogBestWorstAverageFitness(); //Sort population, print to console best / worst fitness and log best, worst and average fitness to log file.
			repopulate = true;//Make sure to repopulate from start in generation 2, if not already set to true, else we will have no evolution.
		}

		if (_saveXMLInSameFile)//ONLY NECESSARY WHEN NOT SAVING AFTER EACH RUN. 
		{ 
			printf("...SAVING FOR THE LAST TIME...\n");
			_ea->SortPopulationAfterFitness();
			_ea->ConvertPopulationToUnitAgentOptimizedProperties();
			_ea->SaveOptimizedPropertiesToXMLFile();
			printf("...SAVING DONE...\n");
		}
		printf("...The EA has finished training. Good bye for this time.\n");
}

void Trainer::Play( vector<UnitAgentOptimizedProperties>& optimizedPropertiesGeneList, 
									 int setGameSpeed, bool setCompleteMapInfo, bool setUserInput )
{
	//BOOST_LOG_SEV(lg(),trace) << "Entered Play";
	while (!Broodwar->isInGame())
	{
		BWAPIClient.update();
		if (!BWAPIClient.isConnected())
		{
			reconnect();
		}
	}
	
	 //wait for game

	Broodwar->setLocalSpeed(setGameSpeed);

	if (setUserInput)
		Broodwar->enableFlag(BWAPI::Flag::UserInput); //Turn User Input On/Off

	if (setCompleteMapInfo)
		Broodwar->enableFlag(BWAPI::Flag::CompleteMapInformation);

	while (Broodwar->isInGame())
	{
		for each (BWAPI::Event e in Broodwar->getEvents())
		{
			using namespace BWAPI;

			switch (e.getType())
			{
			case EventType::MatchStart:
				//Very IMPORTANT. This sets the actual optimized properties to the agent.
				if (!optimizedPropertiesGeneList.empty() && optimizedPropertiesGeneList.size() > 0)
				{
					BotEvents::get().OptimizedPropertiesGeneList = optimizedPropertiesGeneList;
					BotEvents::get().OnStart();
				}
				else
					BOOST_LOG_SEV(lg(),error) << 
					"optimizedPropertiesGeneList is null or has zero elements in OnStart Event";
				break;

			case EventType::MatchEnd:
				Scoring::CalculateFitnessScore();
				BotEvents::get().OnEnd(e.isWinner());
				BOOST_LOG_SEV(lg(),info) << "Game Over. I " << ((e.isWinner()) ? "Won." : "Lost.");
				break;

			case EventType::MatchFrame:
				//if (AnalysisDone && GameEnded == false)
				BotEvents::get().OnFrame();
				break;

			case EventType::MenuFrame:
				break;

			case EventType::SendText:
				BotEvents::get().OnSendText(e.getText());
				break;

			case EventType::ReceiveText:
				//client.onReceiveText(e.player, e.text);
				break;

			case EventType::PlayerLeft:
				//client.onPlayerLeft(e.player);
				break;

			case EventType::NukeDetect:
				//client.onNukeDetect(e.position);
				break;
			case EventType::UnitDiscover:
				// client.onUnitDiscover(e.unit);
				break;
			case EventType::UnitEvade:
				// client.onUnitEvade(e.unit);
				break;

			case EventType::UnitShow:
				if (e.getUnit()->getPlayer() == Broodwar->self())
				{
					BotEvents::get().OnUnitShow(e.getUnit());
					//Broodwar->printf("Unit Shown: [" + e.getUnit()->getType().getName() + "] at [" + e.getUnit()->getPosition().xConst() + "," + e.getUnit()->getPosition().yConst() + "]");
				}
				break;
			case EventType::UnitHide:
				if (e.getUnit()->getPlayer() == Broodwar->self())
				{
					//enemies.Remove(unit);
					BotEvents::get().OnUnitHide(e.getUnit());
					//Broodwar->printf("Unit Hidden: [" + e.getUnit()->getType().getName() + "] at [" + e.getUnit()->getPosition().xConst() + "," + e.getUnit()->getPosition().yConst() + "]");
				}
				break;
			case EventType::UnitCreate:

				if (e.getUnit()->getPlayer() != Broodwar->self())
				{
					//myUnits.Add(new BW.Unit(unit));
					BotEvents::get().OnUnitCreate(e.getUnit());
					// Broodwar->printf("Unit Created: [" + e.getUnit()->getType().getName() + "] at [" + e.getUnit()->getPosition().xConst() + "," + e.getUnit()->getPosition().yConst() + "]");
				}
				break;
			case EventType::UnitDestroy:
				BotEvents::get().OnUnitDestroy(e.getUnit());
				break;
			case EventType::UnitMorph:
				BotEvents::get().OnUnitMorph(e.getUnit());
				break;
			case EventType::UnitRenegade:
				BotEvents::get().OnUnitRenegade(e.getUnit());
				break;
			case EventType::SaveGame:
				BotEvents::get().OnSaveGame(e.getText());
				break;
			default:
				break;
			}
		}

		BWAPIClient.update();
		if (!BWAPIClient.isConnected())
		{
			printf("Reconnecting...\n");
			reconnect();
		}
	}
	//return;
}

void Trainer::reconnect()
{
	while(!BWAPI::BWAPIClient.connect())
	{
		Sleep(1000);
	}
}