#include "TacticalAssaultAgent.h"
#include "cpp_includes.h"
#include "MyMath.h"



void TacticalAssaultAgent::FindBestGoalsForAllUnitsInSquad()//Position position)
{
	if (MySquad.size() > 0)
	{
		for each (pUnitAgent unitAgent in MySquad)
			FindAndSetOptimalGoalForUnitAgent(unitAgent);
		//unitAgent.GoalPosition = SquadMainGoalPosition;
	}
}
void TacticalAssaultAgent::FindAndSetOptimalGoalForUnitAgent(pUnitAgent unitAgent)
{
	if (unitAgent == NULL)
	{
		logfile << "unitAgent is null in method SetGoalPositionForUnitAgentToClosestEnemy";
		throw std::invalid_argument("unitAgent is null");
	}

	Unit* enemyUnitToAttack = MyMath::GetClosestEnemyUnit(unitAgent->MyUnit);

	if (enemyUnitToAttack != NULL)
	{
		unitAgent->GoalUnitToAttack = enemyUnitToAttack;
		unitAgent->GoalPosition = enemyUnitToAttack->getPosition();
	}
}

bool TacticalAssaultAgent::AnyFriendsNear( Unit* unit )
{
	//return MySquad.Any(u => unit.GetDistanceToPosition(u.MyUnit.Position) < 2 && unit.GetDistanceToPosition(u.MyUnit.Position) != 0);
	for each (pUnitAgent u in MySquad)
		if (unit->getDistance(u->MyUnit->getPosition()) < 2 && 
			unit->getDistance(u->MyUnit->getPosition()) != 0)
			return true;
	return false;
}

void TacticalAssaultAgent::ExecuteBestActionForSquad()
{
	FindBestGoalsForAllUnitsInSquad(); // TODO: shouldn't this be outside loop?
	for each (pUnitAgent myUnitAgent in MySquad)
	{
		myUnitAgent->ExecuteBestActionForUnitAgent(MySquad);
	}
}