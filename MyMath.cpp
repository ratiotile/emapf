#include "MyMath.h"
#include "cpp_includes.h"
#include "UnitAgent.h"

int MyMath::_maxDistance = -1;
Position MyMath::_mapCenter =BWAPI::Positions::None;

pUnitAgent MyMath::FindBestLeaderCandidate( vector<pUnitAgent>& squad )
{
	int bestHealth = 0;
	int currentHealth = -1;
	pUnitAgent bestUnitAgent;
	if ( squad.size() > 0)
	{
		bestUnitAgent = squad[0];

		for each (pUnitAgent unitagent in squad)
		{
			if (IsAGoodLeaderCandidate(unitagent->MyUnit))
			{
				currentHealth = unitagent->MyUnit->getHitPoints() + unitagent->MyUnit->getShields();
				if (currentHealth > bestHealth)
				{
					bestHealth = currentHealth;
					bestUnitAgent = unitagent;
				}
			}
		}
		bestUnitAgent->LeadingStatus = LeadingStatusEnum::GroupLeader;
	}
	else
	{
		logfile << "Parameter squad NULL in FindBestLeaderCandidate in MyMath::\n";
	}

	return bestUnitAgent;
}