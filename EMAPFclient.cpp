#include <stdio.h>

#include <BWAPI.h>
#include <BWAPI/Client.h>

#include <windows.h>

#include <string>
#include "cpp_includes.h"
#include "Logger.h"
#include "Settings.h"
#include "Trainer.h"

using namespace BWAPI;

std::ofstream logfile; // logging

void reconnect()
{
	while(!BWAPIClient.connect())
	{
		Sleep(1000);
	}
}

int main(int argc, const char* argv[])
{
	logfile.open("!EMAPFlog.txt",ios::app);
	logfile << "logging begun.\n";
	using namespace logging::trivial;
	BWAPI::BWAPI_init();

	BOOST_LOG_SEV(Logger::get().lg,info) << "Hello World!";
	// initialize conf reader
	Settings::get();
	// initialize trainer
	Trainer::get().run();
	logfile.close();
	/*
	printf("Connecting...");
	reconnect();
	while(true)
	{
		BOOST_LOG_SEV(Logger::get().lg,info) << "waiting to enter match";
		while (!Broodwar->isInGame())
		{
			BWAPI::BWAPIClient.update();
			if (!BWAPI::BWAPIClient.isConnected())
			{
				printf("Reconnecting...\n");
				reconnect();
			}
		}
		BOOST_LOG_SEV(Logger::get().lg,info) << "starting match!" << Broodwar->getFrameCount()
			<< " connected? " << BWAPIClient.isConnected() ;
		Broodwar->sendText("Hello world!");
		while(Broodwar->isInGame())
		{
			for(std::list<Event>::iterator e=Broodwar->getEvents().begin();e!=Broodwar->getEvents().end();e++)
			{
				switch(e->getType())
				{
					case EventType::MatchEnd:
						break;
					case EventType::SendText:
						break;

				}
			}
			if (!Broodwar->isReplay()) // onFrame
			{
				
			}
			BWAPI::BWAPIClient.update();
			if (!BWAPI::BWAPIClient.isConnected())
			{
				printf("Reconnecting...\n");
				reconnect();
			}
		}
	}*/


	return 0;
}