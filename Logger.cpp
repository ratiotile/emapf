#include "Logger.h"
#include <fstream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/utility/empty_deleter.hpp>
#include <boost/log/attributes/scoped_attribute.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/expressions.hpp>
#include <BWAPI.h>
#include <BWAPI/Client.h>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;
namespace expr = boost::log::expressions;

using namespace BWAPI;

Logger::Logger()
{
	// Construct the sink
	typedef sinks::synchronous_sink< sinks::text_ostream_backend > text_sink;
	boost::shared_ptr< text_sink > sink = boost::make_shared< text_sink >();

	// Add a stream to write log to
	boost::shared_ptr<std::ofstream> outStream = 
		boost::make_shared< std::ofstream >("EMAPF.log",std::ofstream::app);
	sink->locked_backend()->add_stream(outStream);
	sink->locked_backend()->auto_flush(true); // flush after every record

	// We have to provide an empty deleter to avoid destroying the global stream object
	boost::shared_ptr< std::ostream > stream(&std::clog, logging::empty_deleter());
	sink->locked_backend()->add_stream(stream);
	
	// set formatter

	sink->set_formatter(
		expr::stream << expr::attr<unsigned int>("LineID")
		<< "," << expr::attr<int>("Frame") << ":<" 
		<< logging::trivial::severity << "> " << expr::smessage
	);

	// Register the sink in the logging core
	logging::core::get()->add_sink(sink);

	// add attributes
	boost::shared_ptr< logging::core > core = logging::core::get();

	core->add_global_attribute("LineID", attrs::counter< unsigned int >(1));
	core->add_global_attribute("TimeStamp", attrs::local_clock());
	//core->add_global_attribute("Scope", attrs::named_scope());
	core->add_global_attribute("Frame", bwapi_frame());
	// filter
}

logging::attribute_value bwapi_frame_impl::get_value()
{
	if( BWAPIClient.isConnected() ) 
		return boost::log::attributes::make_attribute_value(Broodwar->getFrameCount());
	else 
		return boost::log::attributes::make_attribute_value(-1);
}