#include "BotEvents.h"
#include "cpp_includes.h"
#include <boost/make_shared.hpp>
#include "Settings.h"

#include "Terran_Firebat_Agent.h"
#include "Terran_Goliath_Agent.h"
#include "Terran_Marine_Agent.h"
#include "Terran_Medic_Agent.h"
#include "Terran_SCV_Agent.h"
#include "Terran_SiegeTank_Agent.h"
#include "Terran_Vulture_Agent.h"

#include "DrawBuffer.h"

void BotEvents::OnStart()
{
	Broodwar->setGUI(!Settings::get().disableGUI);

	ResetUnitLists(); //Clean all the lists, so we secure that all units in the different lists exists and none is added twice to the same list.
	AddAllUnitsToSeparateLists(); //Add the units to separate lists.
	RanOutOfTime = false;
	_gameRestarted = false;
}

bool BotEvents::OnSendText( string text )
{
	return true;
}

void BotEvents::ResetUnitLists()
{
	_allUnits = Broodwar->getAllUnits();
	_myArmy.clear(); //All the bots army units.
	_myWorkers.clear();
}

pUnitAgent BotEvents::ConvertUnitToUnitAgent( Unit* unit )
{
	using namespace BWAPI::UnitTypes;
	pUnitAgent unitAgent;
	
	if (unit->getType() == Terran_SCV)
		unitAgent = boost::make_shared<UnitAgent>(Terran_SCV_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else if(unit->getType() == Terran_Marine)
		unitAgent = boost::make_shared<UnitAgent>(Terran_Marine_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else if(unit->getType() == Terran_Firebat)
		unitAgent = boost::make_shared<UnitAgent>(Terran_Firebat_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else if(unit->getType() == Terran_Medic)
		unitAgent = boost::make_shared<UnitAgent>(Terran_Medic_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else if(unit->getType() == Terran_Goliath)
		unitAgent = boost::make_shared<UnitAgent>(Terran_Goliath_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else if(unit->getType() == Terran_Vulture)
		unitAgent = boost::make_shared<UnitAgent>(Terran_Vulture_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else if(unit->getType() == Terran_Siege_Tank_Siege_Mode ||
			unit->getType() == Terran_Siege_Tank_Tank_Mode)
		unitAgent = boost::make_shared<UnitAgent>(Terran_SiegeTank_Agent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	else
		unitAgent = boost::make_shared<UnitAgent>(UnitAgent(unit, 
		GetOptimizedValuesToUnitAgent(unit->getType().getName())));
	
	unitAgent->EmotionalMode = EmotionalModeEnum::Exploration;

	return unitAgent;
}

UnitAgentOptimizedProperties BotEvents::GetOptimizedValuesToUnitAgent( string unitTypeName )
{
	if (OptimizedPropertiesGeneList.size() > 0)
	{
			for(vector<UnitAgentOptimizedProperties>::iterator it=OptimizedPropertiesGeneList.begin();
				it != OptimizedPropertiesGeneList.end(); ++it)
		{
			UnitAgentOptimizedProperties& opropTmp = (*it);
			if (!(opropTmp.UnitTypeName.empty()))
			{
				if (opropTmp.UnitTypeName == unitTypeName)
					return opropTmp;
			}
			else
				opropTmp.UnitTypeName = unitTypeName;
		}

		return OptimizedPropertiesGeneList[0];
	}
	BOOST_LOG_SEV(lg(),error)<<"OptimizedPropertiesGeneList was null or had 0 UnitAgentOptimizedProperties in GetOptimizedValuesToUnitAgent in BotEvents.";

	throw std::exception("OptimizedPropertiesGeneList is empty!");
}

void BotEvents::AddAllUnitsToSeparateLists()
{
	vector<pUnitAgent> mySquad;

	for each (Unit* unit in _allUnits) // Game.PlayerSelf.GetUnits IS EQUAL TO MyOwn Units
	{
		if (unit->getPlayer() == Broodwar->self())
		{
			if (unit->getType().isWorker()) //Add all worker units to one workers.
				_myWorkers.push_back(ConvertUnitToUnitAgent(unit));//new UnitAgent(unit));
			else //Add all military units to one squad.
			{
				//_myArmy.Add(ConvertUnitToUnitAgent(unit));
				mySquad.push_back(ConvertUnitToUnitAgent(unit));
			}
		}
	}
	_myArmy.push_back(mySquad);
	// lol why would you make it static then instantiate it??
	// edit: stack ftw; attack damage on the stack: MTG for programmers
	_taa = TacticalAssaultAgent(_myArmy[0]); //Add the only existing squad.
}

void BotEvents::OnFrame()
{
	if (!Broodwar->isInGame() || _gameRestarted)
		return;

	if (Settings::get().training && Broodwar->getFrameCount() > Settings::get().frameLimit)
	{
		if (!RanOutOfTime)
		{
			printf("Ran out of time!\n");
		}
		RanOutOfTime = true;
		Reset();
		return;
	}

	if (Broodwar->getFrameCount() != 0 
		&& Broodwar->getFrameCount() % Settings::get().PFUpdateInterval == 0 
		&& AnyUnitsLeftOnTeams())
	{// && !_gameRestarted && AnyUnitsLeftOnTeams())//&& Game.PlayerSelf.GetUnits().Count > 0)//5
		_taa.ExecuteBestActionForSquad();
	}

	DrawBuffer::get().update();
}

void BotEvents::RemoveDeadUnitFromList( Unit* unit )
{
	if (unit->getPlayer() == Broodwar->self())
	{
		if (unit->getType().isWorker())
		{
			for (vector<pUnitAgent>::iterator i = _myWorkers.begin();
				i != _myWorkers.end(); )
			{
				if ((*i)->MyUnit == unit)
				{
					i = _myWorkers.erase(i);
				}
				else ++i;
			}
		}
		else
		{
			for (vector<vector<pUnitAgent>>::iterator i = _myArmy.begin();
				i != _myArmy.end(); ++i)
			{
				for (vector<pUnitAgent>::iterator j = i->begin(); 
					j != i->end(); )
				{
					if ((*j)->MyUnit->getID() == unit->getID())
					{
						j = i->erase(j);
					}
					else ++j;
				}
			}
			for (vector<pUnitAgent>::iterator i = _taa.MySquad.begin(); 
				i != _taa.MySquad.end(); )
			{
				if ((*i)->MyUnit->getID() == unit->getID())
				{
					i = _taa.MySquad.erase(i);
				}
				else ++i;
			}
		}
	}
}

bool BotEvents::AnyUnitsLeftOnTeams()
{
	//Logger.AddAndPrint("Game.PlayerEnemy.GetUnits().Count: " + Game.PlayerEnemy.GetUnits().Count + "Game.PlayerSelf.GetUnits().Count: " + Game.PlayerSelf.GetUnits().Count);
	return Broodwar->self()->getUnits().size() > 0 
		&& Broodwar->enemy()->getUnits().size() > 0;
}

void BotEvents::OnUnitHide( Unit* unit )
{
	// Util.Logger.Instance.Log("unit hide: " + unit->getType().ToString());

	if (unit->getPlayer() == Broodwar->self() )
	{
		if (unit->getType() == BWAPI::UnitTypes::Terran_Goliath 
			|| unit->getType() == BWAPI::UnitTypes::Terran_Siege_Tank_Siege_Mode 
			|| unit->getType() == BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode )
			RemoveDeadUnitFromList(unit);
	}
}

void BotEvents::Reset()
{
	_allUnits.clear();
	_myArmy.clear(); //All the bots army units.
	// _mySquad.clear();//new List<Unit>();
	_myWorkers.clear();
	_taa = TacticalAssaultAgent();

	//
	OptimizedPropertiesGeneList.clear();
	//

	//System.Threading.Thread.Sleep(100);
	//printf("out of time, restart\n");
	_gameRestarted = true;
	Broodwar->restartGame();
}

void BotEvents::OnEnd( bool isWinner )
{
	if (Settings::get().training && !_gameRestarted)
		Reset();
}

void BotEvents::OnUnitDestroy( Unit* unit )
{
	if (unit->getPlayer() == Broodwar->self())
		RemoveDeadUnitFromList(unit);
}