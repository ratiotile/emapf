#include "EvolutionaryAlgorithm.h"
#include "cpp_includes.h"
#include "OptimalPropertiesSerializer.h"
#include <algorithm>
#include <sstream>
#include "RandomNumber.h"

namespace EvolutionaryAlgorithms{
EvolutionaryAlgorithm::EvolutionaryAlgorithm( int populationSize ):
	PopulationSize(populationSize)
{
	if (populationSize < 10)
		throw std::invalid_argument("Invalid populationSize. The population should at least be 10.");

	InitPopulation();   // Initialise population

	try
	{
		ConvertPopulationToUnitAgentOptimizedProperties();//Convert the Chromosome values to optimized property values that UnitAgent understand.
	}
	catch (std::exception e) { }
}
EvolutionaryAlgorithm::EvolutionaryAlgorithm( int populationSize, 
		int percentOfOpPropsToLoad, const string& dataFileName )
{
	init(populationSize);

	try
	{// Initialise population from an existing file
		//InitPopulationFromExistingFile(25);
		InitPopulationFromExistingFile(percentOfOpPropsToLoad,dataFileName);//100);
	}
	catch (std::exception e) { 
		printf("could not load from file %s\n",dataFileName.c_str());
		throw(e);
	}
}

EvolutionaryAlgorithm::EvolutionaryAlgorithm( int populationSize, string dataFileName )
{
	init(populationSize);

	// Initialise population from an existing file
		OptimizedPropertiesGenes.push_back(LoadGetFittestUAOptPropFromExistingFile(dataFileName));

}

EvolutionaryAlgorithm::EvolutionaryAlgorithm( int populationSize, vector<string> dataFileNames )
{
	init(populationSize);

	try
	{// Initialise population from an existing file
		//InitPopulationFromExistingFile(25);
		for each (string fileName in dataFileNames)
			OptimizedPropertiesGenes.push_back(LoadGetFittestUAOptPropFromExistingFile(fileName));

	}
	catch (std::exception e) {}
}

void EvolutionaryAlgorithm::init(int populationSize)
{
	PopulationSize = populationSize;
}

void EvolutionaryAlgorithm::InitPopulation()
{
	for (size_t i = 0; i < PopulationSize; i++)
	{
		UnitAgentChromosome c;
		c.InitRandomChromosome();
		Population.push_back(c);//The constructor in UAChromosome automatically initializes all the values/genes.
	}
	printf("EvolutionaryAlgorithm: Initialized Population, size = %i\n",Population.size());
}

void EvolutionaryAlgorithm::ConvertPopulationToUnitAgentOptimizedProperties()
{
	OptimizedPropertiesGenes.clear();
	printf("Converting Population to UnitAgentOptimizedProperties\n");

	if (Population.size() > 0)
	{
		sort(Population.begin(),Population.end()); //Population.Sort();

		int rank = 1;
		for each (UnitAgentChromosome chromosome in Population)
		{
			UnitAgentOptimizedProperties opProps = 
				chromosome.ConvertUnitAgentChromosomeToOptimizedValues();
			opProps.Fitness = chromosome.Fitness;
			opProps.Rank = rank;
			OptimizedPropertiesGenes.push_back(opProps);
			rank++;
		}
	}
	else
	{
		logfile << "ERROR: In Method ConvertPopulationToUnitAgentOptimizedProperties in GeneticAlgorithm, the Population was null or had none elements.";
		throw std::exception("ERROR: In Method ConvertPopulationToUnitAgentOptimizedProperties in GeneticAlgorithm, the Population was null or had none elements.");
	}

	if (OptimizedPropertiesGenes.size() != Population.size())
	{
		logfile << "ERROR: In Method ConvertPopulationToUnitAgentOptimizedProperties in GeneticAlgorithm, the OptimizedPropertiesGenes and the Population List didn't have the same number of elements.";
		throw std::exception("ERROR: In Method ConvertPopulationToUnitAgentOptimizedProperties in GeneticAlgorithm, the OptimizedPropertiesGenes and the Population List didn't have the same number of elements.");
	}
}

void EvolutionaryAlgorithm::ConvertUnitAgentOptimizedPropertiesToPopulation()
{
	Population.clear(); //Population = new List<UnitAgentChromosome>();

	//if (OptimizedPropertiesGenes != null && OptimizedPropertiesGenes.size() > 0)
	if(OptimizedPropertiesGenes.size() > 0)
	{
		for each (UnitAgentOptimizedProperties uAOptProp in OptimizedPropertiesGenes)
		{
			UnitAgentChromosome uaChrom;//var uaChrom = new UnitAgentChromosome();
			uaChrom.ConvertAndAddOptimizedValuesToUnitAgentChromosome(uAOptProp); //DeepCopy(uAOptProp));
			Population.push_back(uaChrom);
		}
	}
	else
	{
		logfile << "ERROR: In Method ConvertUnitAgentOptimizedPropertiesToPopulation in GeneticAlgorithm, the OptimizedPropertiesGenes was null or had none elements.";
		throw std::exception("ERROR: In Method ConvertUnitAgentOptimizedPropertiesToPopulation in GeneticAlgorithm, the OptimizedPropertiesGenes was null or had none elements.");
	}

	if (OptimizedPropertiesGenes.size() != Population.size())
	{
		logfile << "ERROR: In Method ConvertUnitAgentOptimizedPropertiesToPopulation in GeneticAlgorithm, the OptimizedPropertiesGenes and the Population List didn't have the same number of elements.";
		throw std::exception("ERROR: In Method ConvertUnitAgentOptimizedPropertiesToPopulation in GeneticAlgorithm, the OptimizedPropertiesGenes and the Population List didn't have the same number of elements.");
	}
}

void EvolutionaryAlgorithm::InitPopulationFromExistingFile( int percentOfOpPropsToLoad, 
																													 const string& filename )
{
	//Loads all the UAOptimizedProperties into a List (Property).
	OptimizedPropertiesGenes = UnitAgentFileHandler.deserialize(filename);
	//if (OptimizedPropertiesGenes != null && OptimizedPropertiesGenes.size() > 0)
	if (OptimizedPropertiesGenes.size() > 0)
	{
		int numberOfOpProps =  (int) floor( 0.5 + (double) (percentOfOpPropsToLoad*OptimizedPropertiesGenes.size()/100));
		if (numberOfOpProps > 100)
			numberOfOpProps = 100;
		else if (numberOfOpProps < 0)
			numberOfOpProps = 0;

		for (int i = 0; i < OptimizedPropertiesGenes.size(); i++)//PopulationSize; i++)
		{
			//Load, convert and add the specified percentage of the already existing optimized values from XML to the corresponding UAChromosomes in the population.
			UnitAgentChromosome uaChromLoaded;
			
			if (i < numberOfOpProps) // 10)
			{
				//UnitAgentOptimizedProperties uAOptProp = OptimizedPropertiesGenes[0];
				
				//var uaChromLoaded = new UnitAgentChromosome();
				//uaChromLoaded.ConvertAndAddOptimizedValuesToUnitAgentChromosome(OptimizedPropertiesGenes[0]);//DeepCopy(OptimizedPropertiesGenes[0]));
				uaChromLoaded.ConvertAndAddOptimizedValuesToUnitAgentChromosome(OptimizedPropertiesGenes[i]);//DeepCopy(OptimizedPropertiesGenes[0]));
				Population.push_back(uaChromLoaded);
			}
			else //The rest of the chromosomes get random values.
			{
				//var c = new UnitAgentChromosome();
				uaChromLoaded.InitRandomChromosome();
				Population.push_back(uaChromLoaded);//The constructor in UAChromosome automatically initializes all the values/genes.
			}
		}
		// correct size of population, in case of loading a smaller or larger pool
		assert(PopulationSize > 0);
		if( Population.size() < PopulationSize )
		{
			// add random
			int numToAdd = PopulationSize - Population.size();
			for (int i = 0; i < PopulationSize; ++i)
			{
				UnitAgentChromosome uaChromLoaded;
				uaChromLoaded.InitRandomChromosome();
				Population.push_back(uaChromLoaded);
			}
		}
		if( Population.size() > PopulationSize ) // truncate
		{
			Population.erase(Population.begin() + PopulationSize, Population.end());
		}
		assert(Population.size() == PopulationSize);
		//Convert and add all the Chromosomes in the population to the UnitAgentOptimizedProperties List.
		ConvertPopulationToUnitAgentOptimizedProperties();
	}
	else
	{
		BOOST_LOG_SEV(lg(),error) << "Cannot find the StarCraft Unit Agent data file. The input OptimizedPropertiesGenes is null.";
		throw std::exception("Cannot find the StarCraft Unit Agent data file. The input OptimizedPropertiesGenes is null.");
	}
}
UnitAgentOptimizedProperties EvolutionaryAlgorithm::LoadGetFittestUAOptPropFromExistingFile( 
	const string& fileName )
{
	//Loads all the UAOptimizedProperties into a List (Property).
	vector<UnitAgentOptimizedProperties> opPropGen = UnitAgentFileHandler.deserialize(fileName);
	//if (OptimizedPropertiesGenes != null && OptimizedPropertiesGenes.size() > 0)
	//if (opPropGen != null && opPropGen.size() > 0)
	if (opPropGen.size() > 0)
		return opPropGen[0];//UnitAgentXMLFileHandler.DeserializeFromXml()[0];//OptimizedPropertiesGenes[0];
	BOOST_LOG_SEV(lg(),error) << "Cannot find the StarCraft Unit Agent data file. \
															 The input OptimizedPropertiesGenes is null.";
	throw std::exception("Cannot find the StarCraft Unit Agent data file. The input OptimizedPropertiesGenes is null.");
}

double EvolutionaryAlgorithm::AverageFitness()
{
	double sum = 0;
	for each(UnitAgentChromosome c in Population)
	{
		sum += (double)c.Fitness;
	}
	//Population.Sum(chromosome => chromosome.Fitness);
	return (double)sum/(double)Population.size();
}

void EvolutionaryAlgorithm::SortPopulationAfterFitness()
{
	if (Population.size() > 0)
	{ 
		sort(Population.begin(),Population.end());//Population.Sort();
		
		std::stringstream bestFitnessStr;
		bestFitnessStr<< "Best " << (Population[0].Fitness) 
			<< " FIRST Value: " << (Population[0].Gene[0]) << " Last value: " 
			<< (Population[0].Gene[Population[0].Gene.size() - 1]);//Population[0].Gene[Population[0].Gene.size() -1] EQUALS LAST ELEMENT/GENE OF THE Chromosome.
		std::stringstream worstFitnessStr;
		worstFitnessStr << "Worst " << Population[Population.size() - 1].Fitness << " " 
			<< Population[Population.size() - 1].Gene[0];

		BOOST_LOG_SEV(lg(),info) << "Best Fitness " << bestFitnessStr.str();
		BOOST_LOG_SEV(lg(),info) << "Worst Fitness " << worstFitnessStr.str();

	}
	else
	{
		logfile << "Population is null in EvolutionaryAlgorithm->SortPopulationAfterFitness or has no values and can't be sorted.";
		throw std::exception("Population is null in EvolutionaryAlgorithm->SortPopulationAfterFitness or has no values and can't be sorted.");
	}
}
void EvolutionaryAlgorithm::LogBestWorstAverageFitness()
{
	SortPopulationAfterFitness();
	double average = AverageFitness();
	BOOST_LOG_SEV(lg(),info) << "LogBestWorstAverageFitness: b: " << Population[0].Fitness
		<< "  w: " << Population[Population.size()-1].Fitness << "  a: " << average
		<< "  m: " << Population[Population.size()/2].Fitness ;
	logfile << Population[0].Fitness<< "," << Population[Population.size()-1].Fitness << "," << average
		<< "," << Population[Population.size()/2].Fitness << "\n";
	logWriteBuffer();
	//for (int i = 0; i < Population.size(); ++i)
	//{
	//	BOOST_LOG_SEV(lg(),info) << " [" << i << "] " << Population[i].Fitness;
	//}
}

void EvolutionaryAlgorithm::Repopulate( int numberOfCrossOvers )
{
	printf("Repopulating, numCrossOvers: %i\n",numberOfCrossOvers);
	if ( Population.size() > 0)
	{
		if ((numberOfCrossOvers % 2) == 0 && numberOfCrossOvers <= Population.size())
		{
			sort(Population.begin(),Population.end());//Population.Sort();
			for (int i = 0; i < Population.size(); ++i)
			{
				BOOST_LOG_SEV(lg(),trace) << " [" << i << "] " << Population[i].Fitness;
			}
			//CloneChromosomeList(Population.GetRange(0, 10));
			//Save the best candidates each time.
			vector<UnitAgentChromosome> newPopulation;
			unsigned int elites = Settings::get().numElites;
			std::copy(Population.begin(),Population.begin()+elites,
				back_inserter(newPopulation));
			assert(newPopulation.size() == elites);
		
			if (numberOfCrossOvers >= 2)
			{
				assert(numberOfCrossOvers <= Population.size() && "Num Crossovers less than Population");
				BOOST_LOG_SEV(lg(),trace) << "Begin crossover";
				//Make one point cross over on the specified best, 
				//and randomly pick the half of the new chromosomes.
				//List<UnitAgentChromosome> newPopulationPart2 = CloneChromosomeList(Population.GetRange(0, numberOfCrossOvers));
				vector<UnitAgentChromosome> newPopulationPart2;
				std::copy(Population.begin(),Population.begin()+numberOfCrossOvers
					,back_inserter(newPopulationPart2));
				assert(newPopulationPart2.size() == numberOfCrossOvers);
				//newPopulation.AddRange(MultiCrossOver(newPopulationPart2, false));
				MultiCrossOver(newPopulationPart2, false);

				newPopulation.insert(newPopulation.end(),newPopulationPart2.begin(),
					newPopulationPart2.end());
			}

			int rest = Population.size() - newPopulation.size(); 
			if (rest > 0)
			{
				vector<UnitAgentChromosome> newPopulationPart3;
				//= CloneChromosomeList(Population.GetRange(0, rest));
				std::copy(Population.begin(),Population.begin()+rest,
					back_inserter(newPopulationPart3));
				//newPopulation.AddRange(MultiMutate(newPopulationPart3));
				//For the rest of the population: mutate
				MultiMutate(newPopulationPart3);

				newPopulation.insert(newPopulation.end(),newPopulationPart3.begin(),
					newPopulationPart3.end());
			}

			//VERY IMPORTANT: Copy the new Population back to the original Population, else there will be no optimization.
			assert(Population.size() == newPopulation.size());
			Population = newPopulation;
			BOOST_LOG_SEV(lg(),trace) << "Repopulation result";
			for (int i = 0; i < Population.size(); ++i)
			{
				BOOST_LOG_SEV(lg(),trace) << " [" << i << "] " << Population[i].Fitness;
			}
			ConvertPopulationToUnitAgentOptimizedProperties();
		}
		else
		{
			logfile << "numberOfCrossOvers  must be an even number and less than the population size.";
			throw exception("numberOfCrossOvers  must be an even number and less than the population size.");
		}
	}
	else
	{
		logfile << "Population is null in EvolutionaryAlgorithm-Repopulate() or has no values.";
		throw exception("Population is null in EvolutionaryAlgorithm->Repopulate() or has no values.");
	}
}

void EvolutionaryAlgorithm::MultiCrossOver( 
	vector<UnitAgentChromosome>& listToCrossover, bool shuffle )
{
	if (listToCrossover.size() % 2 != 0)
		throw std::out_of_range("ERROR in MultiCrossOver: The number of chromosome is odd, but should have been even.");

	if (shuffle)
		RandomNumber::get().ShuffleList(listToCrossover);

	//var crossOverList = new List<UnitAgentChromosome>();
	vector<UnitAgentChromosome> crossOverList;

	for (int i = 0; i < listToCrossover.size()-1; i += 2)
		crossOverList.push_back(CrossOver(listToCrossover[i], listToCrossover[i + 1]));

	// new list is half the size of numToCrossover
	listToCrossover = crossOverList;
}

EvolutionaryAlgorithms::UnitAgentChromosome EvolutionaryAlgorithm::CrossOver( 
	const UnitAgentChromosome& chromo1, const UnitAgentChromosome& chromo2 )
{
	int geneLength = chromo2.Gene.size();
	UnitAgentChromosome newCromosome;//var newCromosome = new UnitAgentChromosome();
	//Should not split on the last index, because then there would not be any newGenePart2.
	int whereToSplit = RandomNumber::get().RandomInt(1, geneLength - 1);
	

	//  Logger.AddAndPrint("whereToSplit: " + whereToSplit);

	int howToCombine = RandomNumber::get().RandomInt(0, 1);
	//Logger.AddAndPrint("howToCombine: " + howToCombine);
	vector<float> newGenePart1;
	vector<float> newGenePart2;

	vector<float> tmpChromosome;//var tmpChromosome = new List<float>();

	if (howToCombine == 0)
	{
		//newGenePart1 = DeepCopy(chromo1.Gene.GetRange(0, whereToSplit + 1));
		std::copy(chromo1.Gene.begin(), chromo1.Gene.begin() + whereToSplit,
			back_inserter(newGenePart1));
		//newGenePart2 = DeepCopy(chromo2.Gene.GetRange(whereToSplit + 1, geneLength - (whereToSplit + 1)));
		std::copy(chromo2.Gene.begin() + whereToSplit, chromo2.Gene.end(), 
			back_inserter(newGenePart2));	
	}
	else
	{
		//newGenePart1 = DeepCopy(chromo2.Gene.GetRange(0, whereToSplit + 1));
		std::copy(chromo2.Gene.begin(), chromo2.Gene.begin() + whereToSplit,
			back_inserter(newGenePart1));
		//newGenePart2 = DeepCopy(chromo1.Gene.GetRange(whereToSplit + 1, geneLength - (whereToSplit + 1)));
		std::copy(chromo1.Gene.begin() + whereToSplit, chromo1.Gene.end(), 
			back_inserter(newGenePart2));
	}
	assert(newGenePart1.size() + newGenePart2.size() == geneLength);
	assert(newGenePart1.size() == whereToSplit);
	assert(newGenePart2.size() == geneLength - (whereToSplit) );
	//tmpChromosome.AddRange(newGenePart1);
	std::copy(newGenePart1.begin(),newGenePart1.end(),back_inserter(tmpChromosome));
	//tmpChromosome.AddRange(newGenePart2);
	std::copy(newGenePart2.begin(),newGenePart2.end(),back_inserter(tmpChromosome));
	assert(tmpChromosome.size() == geneLength);
	newCromosome.Gene = tmpChromosome;
	if (chromo1.Gene.size() != newCromosome.Gene.size())
		throw out_of_range("Invalid number of values in one of the chromosomes in the Crossover.");
	float pc1 = (float)whereToSplit/geneLength;
	newCromosome.Fitness = static_cast<int>(floor(pc1*chromo1.Fitness+(1-pc1)*chromo2.Fitness));
	return newCromosome;
}

void EvolutionaryAlgorithm::MultiMutate( vector<UnitAgentChromosome>& listToMutate )
{
	for(vector<UnitAgentChromosome>::iterator i = listToMutate.begin();
		i != listToMutate.end(); ++i)
	{
		i->MutateGaussianRandom(0.0f, 0.1f); // values are redundant, see RandomNumber constructor
		i->Fitness = 0;
	}
}

void EvolutionaryAlgorithm::SaveOptimizedPropertiesToXMLFile()
{
	//ConvertPopulationToUnitAgentOptimizedProperties();
	//Population.Sort();
// 	try
// 	{
		UnitAgentFileHandler.serialize(OptimizedPropertiesGenes);
//	}
// 	catch (exception e)
// 	{
// 		printf("Error: {0}", e);
// 		//printf(e.StackTrace);
// 		throw;
// 	}
}
}