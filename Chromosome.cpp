#include "chromosome.h"
#include "RandomNumber.h"

using namespace std;

namespace EvolutionaryAlgorithms
{
Chromosome::Chromosome():
	Fitness(0)
{
}

void Chromosome::MutateGaussianRandom( float mean, float deviation )
{
	for (int i = 0; i < Gene.size(); ++i)
	{
		int y = RandomNumber::get().RandomInt(0, 5);
		if (y == 0) {
			float r = RandomNumber::get().NextGaussianf(mean, deviation);
			assert (r == r && "Is not indeterminate");
			Gene[y] += r;
		}
	}
}

void Chromosome::MutateGaussian( float mean, float deviation )
{
	int numberOfRuns = Gene.size();

	for (int y = 0; y < numberOfRuns; y++)
	{
		float r = RandomNumber::get().NextGaussianf(mean, deviation);
		assert (r == r && "Is not indeterminate");
		Gene[y] += r;//r;
	}
}

float Chromosome::FlipFloatRandom( float aFloat )
{
	int r = RandomNumber::get().RandomInt(0,1);
	if(r==0)
		r = -1;
	return aFloat*r;
}

void Chromosome::FlipRandomAllWeights()
{
	for(vector<float>::iterator it = Gene.begin();
		it != Gene.end(); ++it)
	{
		float& f = (*it);
		FlipFloatRandom(f);
	}
}

void Chromosome::SetGene( vector<float> genesFloat )
{
	for each (float f in genesFloat){
		Gene.push_back(f);}
}
}