#include "Scoring.h"
#include "Settings.h"

namespace Scoring
{
	int TotalUnitBuildCount(UnitType unitType)
	{
		return Broodwar->self()->completedUnitCount(unitType) 
			+ Broodwar->self()->deadUnitCount(unitType);
	}
	int TotalUnitBuildScore(UnitType unitType)
	{
		return TotalUnitBuildCount(unitType) * unitType.buildScore();
	}
	/// Calculates the total unit build count for all the units owned by this player.
	int TotalUnitBuildCountForAllOwnUnits()
	{
		//int totalUnitBuildScore
		//foreach (BWAPI.UnitType unitType inSWIG.BWAPI.bwapi.allUnitTypes())
		//totalUnitBuildScore += TotalUnitBuildScore(unitType);
		//return SWIG.BWAPI.bwapi.allUnitTypes().Sum(unitType => TotalUnitBuildScore(unitType));
		int total = 0;
		for each (UnitType unitType in BWAPI::UnitTypes::allUnitTypes()){
			total += TotalUnitBuildScore(unitType);
		}
		return total;
	}
	int TotalUnitDestroyScore(UnitType unitType)
	{
		return Broodwar->self()->killedUnitCount(unitType) * 
			unitType.destroyScore();
	}
	/// Calculates the total unit destroy score for all the units owned by this player.
	int TotalUnitDestroyScoreForAllOwnUnits()
	{
		//return SWIG.BWAPI.bwapi.allUnitTypes().Sum(unitType => TotalUnitDestroyScore(unitType));
		int total = 0;
		for each (UnitType unitType in BWAPI::UnitTypes::allUnitTypes()){
			total += TotalUnitDestroyScore(unitType);
		}
		return total;
	}
	/// The total unit score shown in the end score board.
	int TotalUnitScore()
	{
		return TotalUnitBuildCountForAllOwnUnits() 
			+ TotalUnitDestroyScoreForAllOwnUnits();
	}
	int TotalUnitLossScore(UnitType unitType)
	{
		return Broodwar->self()->deadUnitCount(unitType) * unitType.destroyScore();
	}

	/// Calculates the total unit loss score for all the units owned/lost by this player.
	int TotalUnitLossScoreForAllOwnUnits()
	{
		//return SWIG.BWAPI.bwapi.allUnitTypes().Sum(unitType => TotalUnitLossScore(unitType));
		int total = 0;
		for each (UnitType unitType in BWAPI::UnitTypes::allUnitTypes()){
			total += TotalUnitLossScore(unitType);
		}
		return total;
	}
	int TotalHitpointsLeft()
	{
		//return Broodwar->self()->getUnits().size() > 0 ? 
		//	Broodwar->self()->getUnits().Sum(unit => unit.getHitPoints() + unit.getShields()) : 0;
		int totalHP = 0;
		for each(Unit* unit in Broodwar->self()->getUnits())
		{
			totalHP += unit->getHitPoints() + unit->getShields();
		}
		return totalHP;
	}
	int Fitness = 0;
	void CalculateFitnessScore()
	{
		Fitness = TotalUnitScore() - TotalUnitLossScoreForAllOwnUnits(); 
		//printf("Fitness calculated: %i\n", Fitness);
		if (!BotEvents::get().RanOutOfTime)
			Fitness += TotalHitpointsLeft() * 10; //TotalUnitScore() - TotalUnitLossScoreForAllOwnUnits() + TotalHitpointsLeft() * 10; 
			Fitness += Settings::get().frameLimit - Broodwar->getFrameCount();
	}
}