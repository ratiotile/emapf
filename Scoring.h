#pragma once
#include <BWAPI.h>
#include "Interface.h"
#include "BotEvents.h"

// top half of StarcraftBot.cs

namespace Scoring
{
	int TotalUnitBuildCount(UnitType unitType);
	int TotalUnitBuildScore(UnitType unitType);

	/// Calculates the total unit build count for all the units owned by this player.
	int TotalUnitBuildCountForAllOwnUnits();
	int TotalUnitDestroyScore(UnitType unitType);

	/// Calculates the total unit destroy score for all the units owned by this player.
	int TotalUnitDestroyScoreForAllOwnUnits();

	/// The total unit score shown in the end score board.
	int TotalUnitScore();
	int TotalUnitLossScore(UnitType unitType);

	/// Calculates the total unit loss score for all the units owned/lost by this player.
	int TotalUnitLossScoreForAllOwnUnits();
	int TotalHitpointsLeft();

	extern int Fitness;
	void CalculateFitnessScore();
}