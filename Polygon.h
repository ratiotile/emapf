#pragma once
#include "Interface.h"
#include <math.h>
#include <algorithm>
#include <assert.h>
#include "Triangle.h"
#include <float.h>

class Triangle;

class Polygon
{
public:
	Polygon()
	{
		init();
	}

	Polygon(vector<Position>& points)
	{
		init();
		Points = points;
	}

	void init()
	{
		_mCurrentControlPoint = -1;
		_mNumPoints = 0;
		_mCurrentArea = FLT_MAX;
		_mBestArea = FLT_MAX;
	}

	//vector<Position> Points;
	vector<Position> Points;

	// Find the polygon's centroid.
	Position FindCentroid()
	{
		// Add the first point at the end of the array.
		int nuPoints = Points.size();
		vector<Position> pts(Points);
		pts.push_back(Points[0]);

		// Find the centroid.
		float x = 0;
		float y = 0;
		float secondFactor;
		for (int i = 0; i < nuPoints; i++)
		{
			secondFactor =
				(float)pts[i].x() * pts[i + 1].y() -
				(float)pts[i + 1].x() * pts[i].y();
			x += (pts[i].x() + pts[i + 1].x()) * secondFactor;
			y += (pts[i].y() + pts[i + 1].y()) * secondFactor;
		}

		// Divide by 6 times the polygon's area.
		float polygonArea = PolygonArea();
		x /= (6 * polygonArea);
		y /= (6 * polygonArea);

		// If the values are negative, the polygon is
		// oriented counterclockwise so reverse the signs.
		if (x < 0)
		{
			x = -x;
			y = -y;
		}

		return Position((int)floor( 0.5 + (float)x), (int)floor( 0.5 + (float)y));
	}

	// Return True if the point is in the polygon.
	bool PointInPolygon(float X, float Y)
	{
		// Get the angle between the point and the
		// first and last vertices.
		int maxPoint = Points.size() - 1;
		float totalAngle = GetAngle(
			(float)Points[maxPoint].x(), (float)Points[maxPoint].y(),
			X, Y,
			(float)Points[0].x(), (float)Points[0].y());

		// Add the angles from the point
		// to each other pair of vertices.
		for (int i = 0; i < maxPoint; i++)
		{
			totalAngle += GetAngle(
				(float)Points[i].x(), (float)Points[i].y(),
				X, Y,
				(float)Points[i + 1].x(), (float)Points[i + 1].y());
		}

		// The total angle should be 2 * PI or -2 * PI if
		// the point is in the polygon and close to zero
		// if the point is outside the polygon.
		return (abs(totalAngle) > 0.000001);
	}

///// #region "Orientation Routines"
	// Return True if the polygon is oriented clockwise.
	bool PolygonIsOrientedClockwise()
	{
		return (SignedPolygonArea() < 0);
	}

	// If the polygon is oriented counterclockwise,
	// reverse the order of its points.
	void OrientPolygonClockwise()
	{
		if (!PolygonIsOrientedClockwise())
			reverse(Points.begin(),Points.end());
	}
/////#endregion // Orientation Routines

/// #region "Area Routines"
	// Return the polygon's area in "square units."
	// Add the areas of the trapezoids defined by the
	// polygon's edges dropped to the X-axis. When the
	// program considers a bottom edge of a polygon, the
	// calculation gives a negative area so the space
	// between the polygon and the axis is subtracted,
	// leaving the polygon's area. This method gives odd
	// results for non-simple polygons.
	float PolygonArea()
	{
		// Return the absolute value of the signed area.
		// The signed area is negative if the polyogn is
		// oriented clockwise.
		return abs(SignedPolygonArea());
	}

	// Return the polygon's area in "square units."
	// Add the areas of the trapezoids defined by the
	// polygon's edges dropped to the X-axis. When the
	// program considers a bottom edge of a polygon, the
	// calculation gives a negative area so the space
	// between the polygon and the axis is subtracted,
	// leaving the polygon's area. This method gives odd
	// results for non-simple polygons.
	//
	// The value will be negative if the polyogn is
	// oriented clockwise.
	float SignedPolygonArea()
	{
		// Add the first point to the end.
		int nuPoints = Points.size();
		vector<Position> pts(nuPoints + 1);
		pts.assign(Points.begin(),Points.end());
		pts.push_back(Points[0]);

		// Get the areas.
		float area = 0;
		for (int i = 0; i < nuPoints; i++)
		{
			area +=
				(pts[i + 1].x() - pts[i].x()) *
				(pts[i + 1].y() + pts[i].y()) / 2;
		}

		// Return the result.
		return area;
	}
///#endregion // Area Routines

	// Return True if the polygon is convex.
	bool PolygonIsConvex()
	{
		// For each set of three adjacent points A, B, C,
		// find the dot product AB � BC. If the sign of
		// all the dot products is the same, the angles
		// are all positive or negative (depending on the
		// order in which we visit them) so the polygon
		// is convex.
		bool got_negative = false;
		bool got_positive = false;
		int nuPoints = Points.size();
		int B, C;
		for (int A = 0; A < nuPoints; A++)
		{
			B = (A + 1) % nuPoints;
			C = (B + 1) % nuPoints;

			float cross_product =
				CrossProductLength(
				(float)Points[A].x(), (float)Points[A].y(),
				(float)Points[B].x(), (float)Points[B].y(),
				(float)Points[C].x(), (float)Points[C].y());
			if (cross_product < 0)
			{
				got_negative = true;
			}
			else if (cross_product > 0)
			{
				got_positive = true;
			}
			if (got_negative && got_positive) return false;
		}

		// If we got this far, the polygon is convex.
		return true;
	}

/// #region "Cross and Dot Products"
	// Return the cross product AB x BC.
	// The cross product is a vector perpendicular to AB
	// and BC having length |AB| * |BC| * Sin(theta) and
	// with direction given by the right-hand rule.
	// For two vectors in the X-Y plane, the result is a
	// vector with X and Y components 0 so the Z component
	// gives the vector's length and direction.
	static float CrossProductLength(float Ax, float Ay,
		float Bx, float By, float Cx, float Cy)
	{
		// Get the vectors' coordinates.
		float BAx = Ax - Bx;
		float BAy = Ay - By;
		float BCx = Cx - Bx;
		float BCy = Cy - By;

		// Calculate the Z coordinate of the cross product.
		return (BAx * BCy - BAy * BCx);
	}

	// Return the dot product AB � BC.
	// Note that AB � BC = |AB| * |BC| * Cos(theta).
	static float DotProduct(float Ax, float Ay,
		float Bx, float By, float Cx, float Cy)
	{
		// Get the vectors' coordinates.
		float BAx = Ax - Bx;
		float BAy = Ay - By;
		float BCx = Cx - Bx;
		float BCy = Cy - By;

		// Calculate the dot product.
		return (BAx * BCx + BAy * BCy);
	}
///#endregion // Cross and Dot Products

	// Return the angle ABC.
	// Return a value between PI and -PI.
	// Note that the value is the opposite of what you might
	// expect because Y coordinates increase downward.
	static float GetAngle(float Ax, float Ay, float Bx, float By, float Cx, float Cy)
	{
		// Get the dot product.
		float dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy);

		// Get the cross product.
		float cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, Cy);

		// Calculate the angle.
		return (float) atan2(cross_product, dot_product);
	}

/// #region "Triangulation"
	// Find the indexes of three points that form an "ear."
	void FindEar( int& A, int& B, int& C)
	{
		int nuPoints = Points.size();

		for (A = 0; A < nuPoints; A++)
		{
			B = (A + 1) % nuPoints;
			C = (B + 1) % nuPoints;

			if (FormsEar(Points, A, B, C)) return;
		}

		// We should never get here because there should
		// always be at least two ears.
		assert(false);
	}

	// Return True if the three points form an ear.
	static bool FormsEar(vector<Position> points, int A, int B, int C);

	// Remove an ear from the polygon and
	// add it to the triangles array.
	void RemoveEar(vector<Triangle> triangles);

	// Remove point target from the array.
	void RemovePositionromArray(int target)
	{
		Points.erase(Points.begin() + target);
	}

	// Triangulate the polygon.
	//
	// For a nice, detailed explanation of this method,
	// see Ian Garton's Web page:
	// http://www-cgrl.cs.mcgill.ca/~godfried/teaching/cg-projects/97/Ian/cutting_ears.html
	vector<Triangle> Triangulate();
///#endregion // Triangulation

/// #region "Bounding Rectangle"
	int _mNumPoints;

	// The points that have been used in test edges.
	vector<bool> _mEdgeChecked;

	// The four caliper control points. They start:
	//       m_ControlPoints(0)      Left edge       xmin
	//       m_ControlPoints(1)      Bottom edge     ymax
	//       m_ControlPoints(2)      Right edge      xmax
	//       m_ControlPoints(3)      Top edge        ymin
	int _mControlPoints[4];

	// The line from this point to the next one forms
	// one side of the next bounding rectangle.
	int _mCurrentControlPoint;

	// The area of the current and best bounding rectangles.
	float _mCurrentArea;
	vector<Position> _mCurrentRectangle;
	float _mBestArea;
	vector<Position> _mBestRectangle;
	// object p;


	// Get ready to start.
	void ResetBoundingRect()
	{
		_mNumPoints = Points.size();

		// Find the initial control points.
		FindInitialControlPoints();

		// So far we have not checked any edges.
		_mEdgeChecked = vector<bool>(_mNumPoints);

		// Start with this bounding rectangle.
		_mCurrentControlPoint = 1;
		_mBestArea = FLT_MAX;

		// Find the initial bounding rectangle.
		FindBoundingRectangle();

		// Remember that we have checked this edge.
		_mEdgeChecked[_mControlPoints[_mCurrentControlPoint]] = true;
	}

	// Find the initial control points.
	void FindInitialControlPoints()
	{
		for (int i = 0; i < _mNumPoints; i++)
		{
			if (CheckInitialControlPoints(i)) return;
		}
		assert(false && "Could not find initial control points.");
	}

	// See if we can use segment i --> i + 1 as the base for the initial control points.
	bool CheckInitialControlPoints(int i)
	{
		// Get the i -> i + 1 unit vector.
		int i1 = (i + 1) % _mNumPoints;
		float vix = (float)(Points[i1].x() - Points[i].x());
		float viy = (float)(Points[i1].y() - Points[i].y());

		// The candidate control point indexes.
		for (int num = 0; num < 4; num++)
		{
			_mControlPoints[num] = i;
		}

		// Check backward from i until we find a vector
		// j -> j+1 that points opposite to i -> i+1.
		for (int num = 1; num < _mNumPoints; num++)
		{
			// Get the new edge vector.
			int j = (i - num + _mNumPoints) % _mNumPoints;
			int j1 = (j + 1) % _mNumPoints;
			float vjx = (float)(Points[j1].x() - Points[j].x());
			float vjy = (float)(Points[j1].y() - Points[j].y());

			// Project vj along vi. The length is vj dot vi.
			float dot_product = vix * vjx + viy * vjy;

			// If the dot product < 0, then j1 is
			// the index of the candidate control point.
			if (dot_product < 0)
			{
				_mControlPoints[0] = j1;
				break;
			}
		}

		// If j == i, then i is not a suitable control point.
		if (_mControlPoints[0] == i) return false;

		// Check forward from i until we find a vector
		// j -> j+1 that points opposite to i -> i+1.
		for (int num = 1; num < _mNumPoints; num++)
		{
			// Get the new edge vector.
			int j = (i + num) % _mNumPoints;
			int j1 = (j + 1) % _mNumPoints;
			float vjx = (float)(Points[j1].x() - Points[j].x());
			float vjy = (float)(Points[j1].y() - Points[j].y());

			// Project vj along vi. The length is vj dot vi.
			float dot_product = vix * vjx + viy * vjy;

			// If the dot product <= 0, then j is
			// the index of the candidate control point.
			if (dot_product <= 0)
			{
				_mControlPoints[2] = j;
				break;
			}
		}

		// If j == i, then i is not a suitable control point.
		if (_mControlPoints[2] == i) return false;

		// Check forward from m_ControlPoints[2] until
		// we find a vector j -> j+1 that points opposite to
		// m_ControlPoints[2] -> m_ControlPoints[2]+1.

		i = _mControlPoints[2] - 1;//@
		float temp = vix;
		vix = viy;
		viy = -temp;

		for (int num = 1; num < _mNumPoints; num++)
		{
			// Get the new edge vector.
			int j = (i + num) % _mNumPoints;
			int j1 = (j + 1) % _mNumPoints;
			float vjx = (float)(Points[j1].x() - Points[j].x());
			float vjy = (float)(Points[j1].y() - Points[j].y());

			// Project vj along vi. The length is vj dot vi.
			float dot_product = vix * vjx + viy * vjy;

			// If the dot product <=, then j is
			// the index of the candidate control point.
			if (dot_product <= 0)
			{
				_mControlPoints[3] = j;
				break;
			}
		}

		// If j == i, then i is not a suitable control point.
		if (_mControlPoints[0] == i) return false;

		// These control points work.
		return true;
	}

	// Find the next bounding rectangle and check it.
	void CheckNextRectangle()
	{
		// Increment the current control point.
		// This means we are done with using this edge.
		if (_mCurrentControlPoint >= 0)
		{
			_mControlPoints[_mCurrentControlPoint] =
				(_mControlPoints[_mCurrentControlPoint] + 1) % _mNumPoints;
		}

		// Find the next point on an edge to use.
		float dx0, dy0, dx1, dy1, dx2, dy2, dx3, dy3;
		FindDxDy( dx0,  dy0, _mControlPoints[0]);
		FindDxDy( dx1,  dy1, _mControlPoints[1]);
		FindDxDy( dx2,  dy2, _mControlPoints[2]);
		FindDxDy( dx3,  dy3, _mControlPoints[3]);

		// Switch so we can look for the smallest opposite/adjacent ratio.
		float opp0 = dx0;
		float adj0 = dy0;
		float opp1 = -dy1;
		float adj1 = dx1;
		float opp2 = -dx2;
		float adj2 = -dy2;
		float opp3 = dy3;
		float adj3 = -dx3;

		// Assume the first control point is the best point to use next.
		float bestopp = opp0;
		float bestadj = adj0;
		int best_control_point = 0;

		// See if the other control points are better.
		if (opp1 * bestadj < bestopp * adj1)
		{
			bestopp = opp1;
			bestadj = adj1;
			best_control_point = 1;
		}
		if (opp2 * bestadj < bestopp * adj2)
		{
			bestopp = opp2;
			bestadj = adj2;
			best_control_point = 2;
		}
		if (opp3 * bestadj < bestopp * adj3)
		{
			bestopp = opp3;
			bestadj = adj3;
			best_control_point = 3;
		}

		// Use the new best control point.
		_mCurrentControlPoint = best_control_point;

		// Remember that we have checked this edge.
		_mEdgeChecked[_mControlPoints[_mCurrentControlPoint]] = true;

		// Find the current bounding rectangle
		// and see if it is an improvement.
		FindBoundingRectangle();
	}

	// Find the current bounding rectangle and
	// see if it is better than the previous best.
	void FindBoundingRectangle()
	{
		// See which point has the current edge.
		int i1 = _mControlPoints[_mCurrentControlPoint];
		int i2 = (i1 + 1) % _mNumPoints;
		float dx = (float)(Points[i2].x() - Points[i1].x());
		float dy = (float)(Points[i2].y() - Points[i1].y());
		float temp1,temp2;

		// Make dx and dy work for the first line.
		switch (_mCurrentControlPoint)
		{
		case 0: // Nothing to do.
			break;
		case 1: // dx = -dy, dy = dx
			temp1 = dx;
			dx = -dy;
			dy = temp1;
			break;
		case 2: // dx = -dx, dy = -dy
			dx = -dx;
			dy = -dy;
			break;
		case 3: // dx = dy, dy = -dx
			temp2 = dx;
			dx = dy;
			dy = -temp2;
			break;
		}

		float px0 = (float)Points[_mControlPoints[0]].x();
		float py0 = (float)Points[_mControlPoints[0]].y();
		float dx0 = dx;
		float dy0 = dy;
		float px1 = (float)Points[_mControlPoints[1]].x();
		float py1 = (float)Points[_mControlPoints[1]].y();
		float dx1 = dy;
		float dy1 = -dx;
		float px2 = (float)Points[_mControlPoints[2]].x();
		float py2 = (float)Points[_mControlPoints[2]].y();
		float dx2 = -dx;
		float dy2 = -dy;
		float px3 = (float)Points[_mControlPoints[3]].x();
		float py3 = (float)Points[_mControlPoints[3]].y();
		float dx3 = -dy;
		float dy3 = dx;

		// Find the points of intersection.
		_mCurrentRectangle = vector<Position>(4);
		FindIntersection(px0, py0, px0 + dx0, py0 + dy0, px1, py1, px1 + dx1, py1 + dy1, _mCurrentRectangle[0]);
		FindIntersection(px1, py1, px1 + dx1, py1 + dy1, px2, py2, px2 + dx2, py2 + dy2, _mCurrentRectangle[1]);
		FindIntersection(px2, py2, px2 + dx2, py2 + dy2, px3, py3, px3 + dx3, py3 + dy3, _mCurrentRectangle[2]);
		FindIntersection(px3, py3, px3 + dx3, py3 + dy3, px0, py0, px0 + dx0, py0 + dy0, _mCurrentRectangle[3]);

		// See if this is the best bounding rectangle so far.
		// Get the area of the bounding rectangle.
		float vx0 = (float)(_mCurrentRectangle[0].x() - _mCurrentRectangle[1].x());
		float vy0 = (float)(_mCurrentRectangle[0].y() - _mCurrentRectangle[1].y());
		float len0 = (float)sqrt(vx0 * vx0 + vy0 * vy0);

		float vx1 = (float)(_mCurrentRectangle[1].x() - _mCurrentRectangle[2].x());
		float vy1 = (float)(_mCurrentRectangle[1].y() - _mCurrentRectangle[2].y());
		float len1 = (float)sqrt(vx1 * vx1 + vy1 * vy1);

		// See if this is an improvement.
		_mCurrentArea = len0 * len1;
		if (_mCurrentArea < _mBestArea)
		{
			_mBestArea = _mCurrentArea;
			_mBestRectangle = _mCurrentRectangle;
		}
	}

	// Find the slope of the edge from point i to point i + 1.
	void FindDxDy(float& dx, float& dy, int i)
	{
		int i2 = (i + 1) % _mNumPoints;
		dx = (float)(Points[i2].x() - Points[i].x());
		dy = (float)(Points[i2].y() - Points[i].y());
	}

	// Find the point of intersection between two lines.
	bool FindIntersection(float X1, float Y1, float X2, float Y2, float A1, float B1, float A2, 
		float B2, Position& intersect)
	{
		float dx = X2 - X1;
		float dy = Y2 - Y1;
		float da = A2 - A1;
		float db = B2 - B1;
		float s, t;

		// If the segments are parallel, return False.
		if (abs(da * dy - db * dx) < 0.001) return false;

		// Find the point of intersection.
		s = (dx * (B1 - Y1) + dy * (X1 - A1)) / (da * dy - db * dx);
		t = (da * (Y1 - B1) + db * (A1 - X1)) / (db * dx - da * dy);
		//intersect = new Position(X1 + t * dx, Y1 + t * dy);  //return new Position((int)floor( 0.5 + (float)X1 + t * dx,MidpointRounding.AwayFromZero), (int)floor( 0.5 + (float)Y1 + t * dy,MidpointRounding.AwayFromZero));
		intersect = Position((int)floor( 0.5 + (float)X1 + t * dx), (int)floor( 0.5 + (float)Y1 + t * dy));
		return true;
	}

	// Find a smallest bounding rectangle.
	vector<Position> FindSmallestBoundingRectangle()
	{
		// This algorithm assumes the polygon
		// is oriented counter-clockwise.
		assert(!PolygonIsOrientedClockwise());

		// Get ready;
		ResetBoundingRect();

		// Check all possible bounding rectangles.
		for (size_t i = 0; i < Points.size(); i++)
		{
			CheckNextRectangle();
		}

		// Return the best result.
		return _mBestRectangle;
	}
///#endregion // Bounding Rectangle

};


class Triangle : public Polygon
{
public:
	Triangle(Position p0, Position p1, Position p2)
	{
		Points.resize(3);
		Points[0] = p0;
		Points[1] = p1;
		Points[2] = p2;
	}
};