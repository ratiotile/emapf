#include "Polygon.h"

bool Polygon::FormsEar(vector<Position> points, int A, int B, int C)
{
	// See if the angle ABC is concave.
	if (GetAngle(
		(float)points[A].x(), (float)points[A].y(),
		(float)points[B].x(), (float)points[B].y(),
		(float)points[C].x(), (float)points[C].y()) > 0)
	{
		// This is a concave corner so the triangle
		// cannot be an ear.
		return false;
	}

	// Make the triangle A, B, C.
	Triangle triangle(
		points[A], points[B], points[C]);

	// Check the other points to see 
	// if they lie in triangle A, B, C.
	for (size_t i = 0; i < points.size(); i++)
	{
		if ((i != A) && (i != B) && (i != C))
		{
			if (triangle.PointInPolygon((float)points[i].x(), (float)points[i].y()))
			{
				// This point is in the triangle 
				// do this is not an ear.
				return false;
			}
		}
	}

	// This is an ear.
	return true;
}
void Polygon::RemoveEar(vector<Triangle> triangles)
{
	// Find an ear.
	int A = 0, B = 0, C = 0;
	FindEar( A,  B,  C);

	// Create a new triangle for the ear.
	triangles.push_back(Triangle(Points[A], Points[B], Points[C]));

	// Remove the ear from the polygon.
	RemovePositionromArray(B);
}

vector<Triangle> Polygon::Triangulate()
{
	// Copy the points into a scratch array.
	vector<Position> pts(Points);

	// Make a scratch polygon.
	Polygon pgon(pts);

	// Orient the polygon clockwise.
	pgon.OrientPolygonClockwise();

	// Make room for the triangles.
	vector<Triangle> triangles;

	// While the copy of the polygon has more than
	// three points, remove an ear.
	while (pgon.Points.size() > 3)
	{
		// Remove an ear from the polygon.
		pgon.RemoveEar(triangles);
	}

	// Copy the last three points into their own triangle.
	triangles.push_back( Triangle(pgon.Points[0], pgon.Points[1], pgon.Points[2]));

	return triangles;
}