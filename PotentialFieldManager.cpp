#include "PotentialFieldManager.h"
#include "cpp_includes.h"
#include "DrawBuffer.h"
#include "Settings.h"

using namespace BWAPIext;
using namespace BWAPI;

void PotentialFieldManager::drawUnitWalkTiles()
{
	for each( BWAPI::Unit* unit in Broodwar->getAllUnits() ){
		if (unit->isSelected())
		{
			//want to get an area of a tile to each side of unit.
			BWAPI::TilePosition TL, BR;
			getTileBox(unit,1,TL,BR);
			TilePosition unitTile = TilePosition(unit->getPosition());
			// now get Walktiles
			WalkTile wTL(TL);
			WalkTile wBR(BR);
			wBR.x()+=4;
			wBR.y()+=4;
			// get pixel accurate bound to check against
			Position pTL;
			Position pBR;
			getBoundingBox(unit,pTL,pBR);
			// store the edges of the unit's walktile boundary
			WalkTile wbTL;
			WalkTile wbBR;
			for (int x = wTL.x(); x < wBR.x(); ++x)
			{
				for (int y = wTL.y(); y < wBR.y(); ++y)
				{
					Position iTL(WtoP(x),WtoP(y));
					Position iBR(WtoP(x+1),WtoP(y+1));
					double p = getPercentInside(pTL,pBR,iTL,iBR);
					Color drawcolor = Colors::Red;
					if( p == 1 ) drawcolor = Colors::Green;
					if (p > 0)
					{
						if(wbTL.x() == 0 && wbTL.y() == 0) wbTL = WalkTile(x,y);
						wbBR = WalkTile(x,y);
						Broodwar->drawBoxMap(WtoP(x),WtoP(y),WtoP(x+1),WtoP(y+1),drawcolor);
					}
				}
			}
			// draw ring around boundary in blue, potential field tiles to test
			for (int x = wbTL.x()-1; x < wbBR.x()+2; ++x)
			{
				for (int y = wbTL.y()-1; y < wbBR.y()+2; ++y)
				{
					if ((x == wbTL.x()-1 || x == wbBR.x()+1) || (y==wbTL.y()-1 || y==wbBR.y()+1))
					{
						Broodwar->drawBoxMap(WtoP(x),WtoP(y),WtoP(x+1),WtoP(y+1),Colors::Blue);
					}
				}
			}
			
			// draw tile boundaries
			Broodwar->drawBoxMap(pTL.x(),pTL.y(),pBR.x(),pBR.y(),Colors::Grey);
			Broodwar->drawBoxMap(BtoP(TL.x()),BtoP(TL.y()),BtoP(BR.x()+1),BtoP(BR.y()+1),Colors::Black);
		}
	}
}

double PotentialFieldManager::getPercentInside( const Position oTL, const Position oBR, 
											   const Position iTL, const Position iBR ) const
{
	if (oBR.y() < iTL.y()) return 0;
	if (oBR.x() < iTL.x()) return 0;
	if (oTL.y() > iBR.y()) return 0;
	if (oTL.x() > iBR.x()) return 0;
	// they intersect
	Position uTL(max(iTL.x(),oTL.x()), max(iTL.y(),oTL.y()));
	Position uBR(min(iBR.x(),oBR.x()), min(iBR.y(),oBR.y()));
	// areas
	unsigned int iA = (iBR.x() - iTL.x()) * (iBR.y() - iTL.y());
	unsigned int oA = (oBR.x() - oTL.x()) * (oBR.y() - oTL.y());
	unsigned int uA = (uBR.x() - uTL.x()) * (uBR.y() - uTL.y());
	unsigned int bestA = min(iA,oA);
	if (bestA == 0) return 0;
	return double(uA)/bestA;
}

void PotentialFieldManager::drawUnitBuildTiles()
{
	for each( BWAPI::Unit* unit in Broodwar->getAllUnits() ){
		if (unit->isSelected())
		{
			//want to get an area of a tile to each side of unit.
			BWAPI::TilePosition TL, BR;
			getTileBox(unit,1,TL,BR);
			TilePosition unitTile = TilePosition(unit->getPosition());
			// get pixel accurate bound to check against
			Position pTL;
			Position pBR;
			getBoundingBox(unit,pTL,pBR);
			// draw blue grid of tiles to check PF
			for (int x = unitTile.x()-1; x < unitTile.x()+2; ++x)
			{
				for (int y = unitTile.y()-1; y < unitTile.y()+2; ++y)
				{
					Broodwar->drawBoxMap(BtoP(x),BtoP(y),BtoP(x+1),BtoP(y+1),Colors::Blue);
				}
			}
			Broodwar->drawBoxMap(BtoP(unitTile.x()),BtoP(unitTile.y()),BtoP(unitTile.x())+32,
				BtoP(unitTile.y())+32,Colors::Green);
			Broodwar->drawBoxMap(pTL.x(),pTL.y(),pBR.x(),pBR.y(),Colors::Grey);
			//Broodwar->drawBoxMap(BtoP(TL.x()),BtoP(TL.y()),BtoP(BR.x()+1),BtoP(BR.y()+1),Colors::Black);
		}
	}
}

void PotentialFieldManager::drawUnitBuildTiles2()
{
	for each( BWAPI::Unit* unit in Broodwar->getAllUnits() ){
		if (unit->isSelected())
		{
			//want to get an area of a tile to each side of unit.
			BWAPI::TilePosition TL, BR;
			getTileBox(unit,1,TL,BR);
			TilePosition unitTile = TilePosition(unit->getPosition());
			// get pixel accurate bound to check against
			Position pTL;
			Position pBR;
			getBoundingBox(unit,pTL,pBR);
			//track TL and BR of occupied space
			TilePosition bbTL, bbBR;
			for (int x = unitTile.x()-1; x < unitTile.x()+2; ++x)
			{
				for (int y = unitTile.y()-1; y < unitTile.y()+2; ++y)
				{
					Position iTL(BtoP(x),BtoP(y));
					Position iBR(BtoP(x+1),BtoP(y+1));
					double p = getPercentInside(pTL,pBR,iTL,iBR);
					Color drawcolor = Colors::Red;
					if( p == 1 ) drawcolor = Colors::Green;
					if (p > 0)
					{
						if(bbTL.x() == 0 && bbTL.y() == 0) bbTL = TilePosition(x,y);
						bbBR = TilePosition(x,y);
						Broodwar->drawBoxMap(BtoP(x),BtoP(y),BtoP(x+1),BtoP(y+1),drawcolor);
					}
					//Broodwar->drawBoxMap(BtoP(x),BtoP(y),BtoP(x+1),BtoP(y+1),Colors::Blue);
				}
			}
			// draw ring around boundary in blue, potential field tiles to test
			for (int x = bbTL.x()-1; x < bbBR.x()+2; ++x)
			{
				for (int y = bbTL.y()-1; y < bbBR.y()+2; ++y)
				{
					if ((x == bbTL.x()-1 || x == bbBR.x()+1) || (y==bbTL.y()-1 || y==bbBR.y()+1))
					{
						Broodwar->drawBoxMap(BtoP(x),BtoP(y),BtoP(x+1),BtoP(y+1),Colors::Blue);
					}
				}
			}
			//Broodwar->drawBoxMap(BtoP(unitTile.x()),BtoP(unitTile.y()),BtoP(unitTile.x())+32,
			//	BtoP(unitTile.y())+32,Colors::Green);
			Broodwar->drawBoxMap(pTL.x(),pTL.y(),pBR.x(),pBR.y(),Colors::Grey);
			//Broodwar->drawBoxMap(BtoP(TL.x()),BtoP(TL.y()),BtoP(BR.x()+1),BtoP(BR.y()+1),Colors::Black);
		}
	}
}

void PotentialFieldManager::drawPFTestTiles()
{
	if(drawMode == 0) drawUnitBuildTiles();
	if(drawMode == 1) drawUnitBuildTiles2();
	if(drawMode == 2) drawUnitWalkTiles();
}

std::vector<BWAPI::Position> PotentialFieldManager::getPotentialFieldPositions( BWAPI::Unit* unit )
{
	//want to get an area of a tile to each side of unit.
	Position TL, BR;
	int tilesize = 32;
	Position unitPos = unit->getPosition();
	TL.x() = unitPos.x() - 32;
	TL.y() = unitPos.y() - 32;
	BR.x() = unitPos.x() + 32;
	BR.y() = unitPos.y() + 32;
	vector<Position> positions;
	
	for (int x = TL.x(); x < BR.x()+1; x += 32)
	{
		for (int y = TL.y(); y < BR.y()+1; y += 32)
		{
			if (x == TL.x() || x == BR.x() || y == TL.y() || y == BR.y())
			{
				Position p(x,y);
				if(p.isValid()) positions.push_back(Position(x,y));
			}
		}
	}
	// debug draw
	if(unit->isSelected()){
		drawCirclePoints(positions);
	}
	return positions;
}

double PotentialFieldManager::CalculatePFOwnUnitRepulsion( BWAPI::Unit* unit, BWAPI::Position field,
	BWAPI::Unit* myOtherUnit, double force, double forceStep, int range )
{
	double distance = myOtherUnit->getDistance(field);
	if (unit->getID() == myOtherUnit->getID() || unit->getType().isFlyer() || myOtherUnit->getType().isFlyer()
		|| unit->getType().isFlyingBuilding() || myOtherUnit->getType().isFlyingBuilding())
	{
		//If both units has the same ID, there can't be a collision, so don't calculate anything.
		return 0;
	}

	if (myOtherUnit->getType() == UnitTypes::Terran_Vulture_Spider_Mine)
		if (distance <= 125 + GetUnitSize(unit->getType()))//Avoid Spider Mines.
			return CalculateRepulsiveMagnitude(force, forceStep, distance, false);//-20;

	if (unit->isCloaked() && !myOtherUnit->isCloaked()) //Cloak units should avoid standing to close to other non cloaked units to avoid splash damage from tanks for instance.
		if (distance <= 50 + GetUnitSize(unit->getType()))
			return CalculateRepulsiveMagnitude(force, forceStep, distance, false);//-20;

	//OPTIMIZE BY EVOLUTIONARY COMPUTATION
	return distance <= GetUnitSize(unit->getType()) + range ? CalculateRepulsiveMagnitude(force, forceStep, distance, false) : 0;
}

int PotentialFieldManager::GetUnitSize( UnitType type )
{
	if (type.isWorker())
		return 6;
	if (type.size() == UnitSizeTypes::Small)
		return 6;
	if (type.size() ==  UnitSizeTypes::Medium)
		return 12;
	if (type.size() == UnitSizeTypes::Large)
		return 16;
	return 12;
}

double PotentialFieldManager::CalculateRepulsiveMagnitude( double force, double forceStep, double distance, bool canBePositive )
{
	if (!canBePositive)
	{
		double magnitude = -(force - forceStep * distance);
		return magnitude < 0 ? magnitude : 0;
	}
	return -(force - forceStep * distance);
}

void PotentialFieldManager::drawCirclePoints( const vector<BWAPI::Position>& points ) const
{
	BOOST_FOREACH(Position p, points)
	{
		DrawBuffer::get().drawBufferedCircle(CoordinateType::Map,p.x(),p.y(),15,
			Settings::get().PFUpdateInterval,Colors::Grey);
	}
}