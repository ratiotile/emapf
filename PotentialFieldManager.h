#pragma once
#include "Singleton.h"
#include "Interface.h"
#include <BWAPI.h>

class PotentialFieldManager: public Singleton<PotentialFieldManager>
{
public:
	PotentialFieldManager(){drawMode = 0;}

	std::vector<BWAPI::Position> getPotentialFieldPositions(BWAPI::Unit* unit);
	std::vector<BWAPI::Position> getRotatedSurroundingPositions(BWAPI::Unit* unit
		, int myTileSize, int newAngle);

	double CalculatePFOwnUnitRepulsion(BWAPI::Unit* unit, BWAPI::Position field, BWAPI::Unit* myOtherUnit, 
		double force, double forceStep, int range);

	int GetUnitSize(BWAPI::UnitType type);

	double CalculateRepulsiveMagnitude(double force, double forceStep, double distance, bool canBePositive);

	void drawUnitWalkTiles();
	double getPercentInside(const Position oTL, const Position oBR, const Position iTL, const Position iBR) const;
	void drawUnitBuildTiles();
	void drawUnitBuildTiles2();
	void drawPFTestTiles();
	void drawCirclePoints(const vector<BWAPI::Position>& points) const;
	int drawMode;
};