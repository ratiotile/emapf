#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_Marine_Agent : public UnitAgent
{
public:
	Terran_Marine_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_Marine_Agent";
		//SightRange = 7; 
	}
};
typedef boost::shared_ptr<Terran_Marine_Agent> pTerran_Marine_Agent;