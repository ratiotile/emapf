#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_Firebat_Agent : public UnitAgent
{
public:
	Terran_Firebat_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_Firebat_Agent";
		//SightRange = 7; 
	}
};
typedef boost::shared_ptr<Terran_Firebat_Agent> pTerran_Firebat_Agent;