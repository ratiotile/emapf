#pragma once
#include <boost/log/core.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/logger.hpp>
#include "Singleton.h"
#include <boost/log/sources/record_ostream.hpp> //BOOST_LOG
#include <boost/log/trivial.hpp>
#include <boost/log/attributes/attribute_cast.hpp>
#include <boost/log/attributes/attribute.hpp>
#include <boost/log/attributes/attribute_value.hpp>
#include <boost/log/attributes/attribute_value_impl.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;

class Logger : public Singleton<Logger>
{
public:
	Logger();


	boost::log::sources::severity_logger<logging::trivial::severity_level> lg;

};

///  attribute to print out current frame, or -1 if not in game
// attribute implementation class
class bwapi_frame_impl: public logging::attribute::impl
{
public:
	logging::attribute_value get_value();
};
// attribute interface class
class bwapi_frame: public logging::attribute
{
public:
	bwapi_frame(): logging::attribute(new bwapi_frame_impl()){}
	// attribute casting support
	explicit bwapi_frame(boost::log::attributes::cast_source const& source) :
		logging::attribute(source.as<bwapi_frame_impl>()){}
};