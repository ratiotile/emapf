//EvolutionaryAlgorithm.cs
#include "Interface.h"
#include "OptimalPropertiesSerializer.h"
#include "UnitAgentChromosome.h"
#include "UnitAgentOptimizedProperties.h"

namespace EvolutionaryAlgorithms{

class EvolutionaryAlgorithm
{
public:
	OptimalPopertiesSerializer UnitAgentFileHandler;
	vector<UnitAgentChromosome> Population;
	unsigned int PopulationSize;
	vector<UnitAgentOptimizedProperties> OptimizedPropertiesGenes;

	EvolutionaryAlgorithm(int populationSize);
	EvolutionaryAlgorithm(int populationSize, int percentOfOpPropsToLoad, const string& dataFileName);
	EvolutionaryAlgorithm(int populationSize, string dataFileName);
	EvolutionaryAlgorithm(int populationSize, vector<string> dataFileNames);
	void init(int populationSize);
	void InitPopulation();

	void ConvertPopulationToUnitAgentOptimizedProperties();
	void ConvertUnitAgentOptimizedPropertiesToPopulation();
	void InitPopulationFromExistingFile(int percentOfOpPropsToLoad, const string& filename);

	UnitAgentOptimizedProperties LoadGetFittestUAOptPropFromExistingFile(const string& fileName);

	void LogBestWorstAverageFitness();
	/// Calculates the average fitness of all chromosomes in the population.
	double AverageFitness();
	/// Sort the population after the genes fitness and print best and worst to the console.   
	void SortPopulationAfterFitness();

	/// Repopulates the population (executes variation operations and creates the new population by selecting the fittest (also called: Survivor Selection or replacement)). 
	/// After 1 run remember to call ConvertPopulationToUnitAgentOptimizedProperties() 
	/// before running this method.
	void Repopulate(int nuberOfCrossOvers);

	///  Takes a list where all chromosomes should be crossed over. 
	///  The method returns a new list with the crossed over chromosomes.
	///  The new list is half the size as the input list. 
	void MultiCrossOver( vector<UnitAgentChromosome>& listToCrossover, bool shuffle);

	/// Creates a one-point crossover between 2 chromosomes. It creates 
	/// a new chromosome combined of the 2 input chromosomes, by splitting the
	/// 2 chromosomes up at a random place and combine the two parts randomly.
	UnitAgentChromosome CrossOver(
		const UnitAgentChromosome& chromo1, const UnitAgentChromosome& chromo2);

	/// Mutate randomly different values from all Chromosomes in a List.
	void MultiMutate( vector<UnitAgentChromosome>& listToMutate);

	/// Serialize and save all the optimized properties into an XML file. 
	/// Very IMPORTANT: Remember to call the ConvertPopulationToUnitAgentOptimizedProperties 
	/// before calling this method, else it will not be the newest optimized properties that
	/// will be saved.
	void SaveOptimizedPropertiesToXMLFile();
};

} 