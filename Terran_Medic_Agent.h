#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_Medic_Agent : public UnitAgent
{
public:
	Terran_Medic_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_Medic_Agent";
		//SightRange = 7; 
	}
};
typedef boost::shared_ptr<Terran_Medic_Agent> pTerran_Medic_Agent;