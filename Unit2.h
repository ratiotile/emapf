#pragma once
#include <BWAPI.h>

class Unit
{
public:
	BWAPI::Unit* theUnit;
	BWAPI::Unit* theTarget;

	Unit(BWAPI::Unit* u)
		:theUnit(u),theTarget(NULL)
	{}
	bool HasTarget() const { return theTarget != NULL; }
	void Attack(BWAPI::Unit* t)
	{
		if (t == NULL && theTarget != NULL)
		{
			theUnit->stop();
			theTarget = NULL;
		}
		else if (t != NULL && t != theTarget)
		{
			theTarget = t;
			theUnit->attack(theTarget);
		}
	}
};