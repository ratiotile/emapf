#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_SiegeTank_Agent : public UnitAgent
{
public:
	Terran_SiegeTank_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_SiegeTank_Agent";
		//SightRange = 10; 
	}
};
typedef boost::shared_ptr<Terran_SiegeTank_Agent> pTerran_SiegeTank_Agent;