#pragma once
#include "Chromosome.h"
#include "UnitAgentOptimizedProperties.h"
#include "Interface.h"
#include <math.h>

namespace EvolutionaryAlgorithms
{
class UnitAgentChromosome: public Chromosome
{
	static const int NumberOfUnitAgentOptimizedProperties = 19;
public:
	UnitAgentChromosome();
	UnitAgentChromosome(std::vector<float> gene);
	// copy constructor
	UnitAgentChromosome(const UnitAgentChromosome& toCopy);

	// return 0 if equal, <0 if this is less, >0 if this is greater
	int CompareTo(const UnitAgentChromosome& other) const;

	void ConvertAndAddOptimizedValuesToUnitAgentChromosome(UnitAgentOptimizedProperties opProp);

	/// Checks if a value_multiplyer is zero, then return zero else return value divided by value_multiplyer.
private: float ConvertOptimizedValueToUnitAgentChromosome(float value, int valueMultiplyer);

public: 
	UnitAgentOptimizedProperties ConvertUnitAgentChromosomeToOptimizedValues();
	void InitRandomChromosome();
};

bool operator< (const UnitAgentChromosome& left, const UnitAgentChromosome& right);

}