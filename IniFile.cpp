#include "IniFile.h"
#include "cpp_includes.h"

IniFile::IniFile() :
filepath("data/settings.info")
{
	try
	{
		load();
		BOOST_LOG_SEV(lg(),trace) << "Settings.info successfully read!";
	}
	catch (std::exception* e)
	{
		BOOST_LOG_SEV(lg(),error) << "IniFile error: " << e->what();
	}
}

void IniFile::load()
{
	read_info(filepath, pt_);
}

void IniFile::save()
{
	write_info(filepath, pt_);
}

template<typename Type>
Type IniFile::read( const string& path )
{
	Type retVal;
	try{
		retVal = pt_.get<Type>(path);
	}
	catch(boost::property_tree::ptree_error pe)
	{
		BOOST_LOG_SEV(lg(),error) << "Error: " << pe.what();
		throw(pe);
	}
	return retVal;
}
template int IniFile::read( const string& path);
template unsigned int IniFile::read( const string& path);
template bool IniFile::read( const string& path);
template string IniFile::read( const string& path);