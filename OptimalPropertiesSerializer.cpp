#include "OptimalPropertiesSerializer.h"
#include "cpp_includes.h"
#include <fstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <sys/stat.h>
#include "UnitAgentOptimizedProperties.h"
#include <boost/serialization/vector.hpp>

using namespace std;

void OptimalPopertiesSerializer::serialize( const vector<UnitAgentOptimizedProperties>& data )
{
	ofstream fo;
	fo.open( Filename.c_str() );
	boost::archive::text_oarchive oa(fo);
	oa << data;
	fo.close();
}

vector<UnitAgentOptimizedProperties> OptimalPopertiesSerializer::deserialize()
{
	vector<UnitAgentOptimizedProperties> data;
	ifstream fi;
	fi.open( Filename.c_str() );
	boost::archive::text_iarchive ia(fi);
	ia >> data;
	fi.close();
	return data;
}

vector<UnitAgentOptimizedProperties> OptimalPopertiesSerializer::deserialize(
	const string& filename)
{
	vector<UnitAgentOptimizedProperties> data;
	ifstream fi;
	fi.open( filename.c_str() );
	boost::archive::text_iarchive ia(fi);
	ia >> data;
	fi.close();
	return data;
}
bool OptimalPopertiesSerializer::fileExists( std::string filename )
{
	struct stat stFileInfo;
	return stat(filename.c_str(),&stFileInfo) == 0;
}