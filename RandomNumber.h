#pragma once
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <math.h>
#include <time.h>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
#include <random>

#include "Singleton.h"

class RandomNumber : public Singleton<RandomNumber>
{
public:
	boost::random::mt19937 gen; // mersenne twister random numbers
	boost::random::normal_distribution<double> normDist;
	boost::random::normal_distribution<float> normDistf;

	RandomNumber():
		gen((unsigned int)time(NULL)),
		normDist(0.0,0.1),
		normDistf(0.0f,0.1f)
	{}
	/// high must be >= low
	int RandomInt(int low, int high){
		boost::random::uniform_int_distribution<int> dist(low,high);
		return dist(gen);
	}

	float RandomFloat(float low, float high)
	{
		boost::random::uniform_real_distribution<float> dist(low,high);
		return dist(gen);
	}

	double RandomDouble(double low, double high)
	{
		boost::random::uniform_real_distribution<double> dist(low,high);
		return dist(gen);
	}

	bool Odd(int o)
	{
		return ((o & 1) == 1);
	}

	bool Even(int e)
	{
		return ((e & 1) == 0);
	}
	int shuffleRand(int i)
	{
		return RandomInt(0,i-1); //0 to n-1 required by random_shuffle
	}
	template<typename E>
	void ShuffleList(std::vector<E>& list)
	{
		if (list.size() <= 1) return;
		boost::random::mt19937& rgen = gen;
		random_shuffle(list.begin(),list.end(),std::tr1::bind(&RandomNumber::shuffleRand
			,this,std::tr1::placeholders::_1));
	}
	

	// gaussian stuff
	double NextGaussian(double dMu, double dSigma)
	{
		return normDist(gen);
	}
	
	float NextGaussianf(float dMu, float dSigma)
	{
		return normDistf(gen);
	}
	
};