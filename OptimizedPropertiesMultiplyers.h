#pragma once
#include "Singleton.h"

class OptimizedPropertiesMultiplyers: public Singleton<OptimizedPropertiesMultiplyers>
{
	friend class Singleton<OptimizedPropertiesMultiplyers>;
	OptimizedPropertiesMultiplyers()
	{
		//Force multiplyers
		ForceOwnUnitsRepulsionMultiplyer = 1000;
		ForceOwnUnitsRepulsionMultiplyer = 1000;
		ForceEnemyUnitsRepulsionMultiplyer = 1000;
		ForceMSDMultiplyer = 1000;
		ForceSquadAttractionMultiplyer = 1000;
		ForceMapCenterAttractionMultiplyer = 1000;
		ForceMapEdgeRepulsionMultiplyer = 1000;
		ForceWeaponCoolDownRepulsionMultiplyer = 1000;

		//Step is used for each distance step (slope).
		ForceStepOwnUnitsRepulsionMultiplyer = 10;
		ForceStepEnemyUnitsRepulsionMultiplyer = 10;
		ForceStepMSDMultiplyer = 10;
		ForceStepSquadAttractionMultiplyer = 10;
		ForceStepMapCenterAttractionMultiplyer = 10;
		ForceStepMapEdgeRepulsionMultiplyer = 10;
		ForceStepWeaponCoolDownRepulsionMultiplyer = 10;

		//Range multiplyers
		RangeOwnUnitsRepulsionMultiplyer = 512;
		RangePercentageSquadAttractionMultiplyer = 100; //PERCENTAGE
		RangePecentageMapCenterAttractionMultiplyer = 100; //PERCENTAGE
		RangeMapEdgeRepulsionMultiplyer = 512;//0-320
		RangeWeaponCooldownRepulsionMultiplyer = 512;//= 160;
	}
public:
	int ForceOwnUnitsRepulsionMultiplyer; //= 100;//-500; // 0 - 1000
	int ForceEnemyUnitsRepulsionMultiplyer; //= 200; // 0 - 1000 //REMEMBER TO SET TO A POSITIVE NUMBER. 
	int ForceMSDMultiplyer; //= 240; //0-2000 or 1000  ....800
	int ForceSquadAttractionMultiplyer; //= 10; //0-1000
	int ForceMapCenterAttractionMultiplyer; //= 20; //0-1000
	int ForceMapEdgeRepulsionMultiplyer; //= 50;//0-1000
	int ForceWeaponCoolDownRepulsionMultiplyer; //= 800;//0-1000

	//Step is used for each distance step (slope).
	int ForceStepOwnUnitsRepulsionMultiplyer; //= 0.6; // 0 - 10 SMALLER THAN punishmentRepulsionOwnUnits
	int ForceStepEnemyUnitsRepulsionMultiplyer; //= 1.2; // 0 - 10
	int ForceStepMSDMultiplyer; //= 0.24;//5; //0-10
	int ForceStepSquadAttractionMultiplyer; //= 0.1; //0 - 10
	int ForceStepMapCenterAttractionMultiplyer; // = 0.09; // 0 - 10
	int ForceStepMapEdgeRepulsionMultiplyer; //= 0.3;// 0 - 10
	int ForceStepWeaponCoolDownRepulsionMultiplyer; // = 6.4;// 0 - 10

	int RangeOwnUnitsRepulsionMultiplyer; //= 8; //0-512    20
	int RangePercentageSquadAttractionMultiplyer; //= 5;//15; //0-100  //SHOULD SOMEHOW DEPEND ON HOW BIG THE SQUAD IS
	int RangePecentageMapCenterAttractionMultiplyer; // = 5; //0-100
	int RangeMapEdgeRepulsionMultiplyer; //= 96; //0-320
	int RangeWeaponCooldownRepulsionMultiplyer; //= 160;
};