#include "UnitAgentOptimizedProperties.h"
#include "cpp_includes.h"

// flip to put highest first
bool operator< (const UnitAgentOptimizedProperties& left,
				const UnitAgentOptimizedProperties& right)
{
	return left.CompareTo(right) > 0;
}