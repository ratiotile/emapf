#pragma once
#include "Interface.h"
#include "UnitAgent.h"

//TacticalAssaultAgent.cs
/// Tactical Assault Agent class...
/// @author Thomas Willer Sandberg (http://twsandberg.dk/)
/// @version (1.0, January 2011)
class TacticalAssaultAgent
{
public:
	vector<pUnitAgent> MySquad;

	TacticalAssaultAgent(vector<pUnitAgent>& mySquad):MySquad(mySquad){}
	TacticalAssaultAgent(){}

	void FindBestGoalsForAllUnitsInSquad();
	void FindAndSetOptimalGoalForUnitAgent(pUnitAgent unitAgent);

	 /// Checks if there are any units near the specified unit.
	bool AnyFriendsNear(Unit* unit);

	/// The tactical assault agent will deside which action that would be best for the squad.
	/// For instance find and attack closest enemy units using the squad (team) of own units. 
	/// A tactic could be to attack the units with lowest health first, and using medics to 
	/// heal the most injured melee units first.
	void ExecuteBestActionForSquad();
};