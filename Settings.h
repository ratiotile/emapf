#pragma once
#include "Interface.h"
#include "Singleton.h"

class Settings : public Singleton<Settings>
{
public:
	Settings();
	const bool Debug;
	const string SerializeName;
	const string LastChanceHandlerError;
	const int frameLimit;

	unsigned int totalNumberOfRepopulations; // total number of generations
	unsigned int startGenerationOn; // current progress
	unsigned int percentOfOpPropsToLoad;
	unsigned int populationSize;
	unsigned int numberOfTrials;
	unsigned int numberOfCrossovers;
	unsigned int numElites; // elitist selection
	string loadFileName;
	string runOption;
	int gameSpeed;
	bool training;
	bool disableGUI;
	bool userSelectedControl; // enable user to control selected units
	bool displaySelectedPF; // show PF around selected units.
	unsigned int PFUpdateInterval; // Frames inbetween PF updates
};