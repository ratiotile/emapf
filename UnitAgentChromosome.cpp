#include "UnitAgentChromosome.h"
#include "cpp_includes.h"
#include "OptimizedPropertiesMultiplyers.h"
#include <stdexcept>
#include "RandomNumber.h"

using namespace std;

namespace EvolutionaryAlgorithms
{
	UnitAgentChromosome::UnitAgentChromosome()
		:Chromosome()
	{	}
	UnitAgentChromosome::UnitAgentChromosome( std::vector<float> gene )
		:Chromosome(gene)
	{}

	UnitAgentChromosome::UnitAgentChromosome( const UnitAgentChromosome& toCopy )
	{
		Gene =  vector<float>(toCopy.Gene);
		Fitness = toCopy.Fitness;
	}

	int UnitAgentChromosome::CompareTo( const UnitAgentChromosome& other ) const
	{
		if(Fitness > other.Fitness) return 1;
		if(Fitness < other.Fitness) return -1;
		return 0;
	}

	void UnitAgentChromosome::ConvertAndAddOptimizedValuesToUnitAgentChromosome( UnitAgentOptimizedProperties opProp )
	{
		if (Gene.size() > 0)
		{
			logfile << "WARNING: There was already values in the UnitAgentChromosome, "
				<< "when it was initialized. They have been deleted\n";
		}

		Fitness = opProp.Fitness;

		//Force
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceOwnUnitsRepulsion, OptimizedPropertiesMultiplyers::get().ForceOwnUnitsRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceEnemyUnitsRepulsion, OptimizedPropertiesMultiplyers::get().ForceEnemyUnitsRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceMSD, OptimizedPropertiesMultiplyers::get().ForceMSDMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceSquadAttraction, OptimizedPropertiesMultiplyers::get().ForceSquadAttractionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceMapCenterAttraction, OptimizedPropertiesMultiplyers::get().ForceMapCenterAttractionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceMapEdgeRepulsion, OptimizedPropertiesMultiplyers::get().ForceMapEdgeRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceWeaponCoolDownRepulsion, OptimizedPropertiesMultiplyers::get().ForceWeaponCoolDownRepulsionMultiplyer));

		//ForceStep
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepOwnUnitsRepulsion, OptimizedPropertiesMultiplyers::get().ForceStepOwnUnitsRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepEnemyUnitsRepulsion, OptimizedPropertiesMultiplyers::get().ForceStepEnemyUnitsRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepMSD, OptimizedPropertiesMultiplyers::get().ForceStepMSDMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepSquadAttraction, OptimizedPropertiesMultiplyers::get().ForceStepSquadAttractionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepMapCenterAttraction, OptimizedPropertiesMultiplyers::get().ForceStepMapCenterAttractionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepMapEdgeRepulsion, OptimizedPropertiesMultiplyers::get().ForceStepMapEdgeRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.ForceStepWeaponCoolDownRepulsion, OptimizedPropertiesMultiplyers::get().ForceStepWeaponCoolDownRepulsionMultiplyer));

		//Range
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.RangeOwnUnitsRepulsion, OptimizedPropertiesMultiplyers::get().RangeOwnUnitsRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.RangePercentageSquadAttraction, OptimizedPropertiesMultiplyers::get().RangePercentageSquadAttractionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.RangePecentageMapCenterAttraction, OptimizedPropertiesMultiplyers::get().RangePecentageMapCenterAttractionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.RangeMapEdgeRepulsion, OptimizedPropertiesMultiplyers::get().RangeMapEdgeRepulsionMultiplyer));
		Gene.push_back(ConvertOptimizedValueToUnitAgentChromosome((float)opProp.RangeWeaponCooldownRepulsion, OptimizedPropertiesMultiplyers::get().RangeWeaponCooldownRepulsionMultiplyer));
	}

	float UnitAgentChromosome::ConvertOptimizedValueToUnitAgentChromosome( float value, int valueMultiplyer )
	{
		if (valueMultiplyer != 0)
			return value / (float)valueMultiplyer;
		return 0;
	}

	UnitAgentOptimizedProperties UnitAgentChromosome::ConvertUnitAgentChromosomeToOptimizedValues()
	{
		if (Gene.size() > 0 && Gene.size() == NumberOfUnitAgentOptimizedProperties)
		{
			UnitAgentOptimizedProperties opProp;

			//Force
			opProp.ForceOwnUnitsRepulsion = Gene[0] * OptimizedPropertiesMultiplyers::get().ForceOwnUnitsRepulsionMultiplyer;
			opProp.ForceEnemyUnitsRepulsion = Gene[1] * OptimizedPropertiesMultiplyers::get().ForceEnemyUnitsRepulsionMultiplyer;
			opProp.ForceMSD = Gene[2] * OptimizedPropertiesMultiplyers::get().ForceMSDMultiplyer;
			opProp.ForceSquadAttraction = Gene[3] * OptimizedPropertiesMultiplyers::get().ForceSquadAttractionMultiplyer;
			opProp.ForceMapCenterAttraction = Gene[4] * OptimizedPropertiesMultiplyers::get().ForceMapCenterAttractionMultiplyer;
			opProp.ForceMapEdgeRepulsion = Gene[5] * OptimizedPropertiesMultiplyers::get().ForceMapEdgeRepulsionMultiplyer;
			opProp.ForceWeaponCoolDownRepulsion = Gene[6] * OptimizedPropertiesMultiplyers::get().ForceWeaponCoolDownRepulsionMultiplyer;

			//ForceStep
			opProp.ForceStepOwnUnitsRepulsion = Gene[7] * OptimizedPropertiesMultiplyers::get().ForceStepOwnUnitsRepulsionMultiplyer;
			opProp.ForceStepEnemyUnitsRepulsion = Gene[8] * OptimizedPropertiesMultiplyers::get().ForceStepEnemyUnitsRepulsionMultiplyer;
			opProp.ForceStepMSD = Gene[9] * OptimizedPropertiesMultiplyers::get().ForceStepMSDMultiplyer;
			opProp.ForceStepSquadAttraction = Gene[10] * OptimizedPropertiesMultiplyers::get().ForceStepSquadAttractionMultiplyer;
			opProp.ForceStepMapCenterAttraction = Gene[11] * OptimizedPropertiesMultiplyers::get().ForceStepMapCenterAttractionMultiplyer;
			opProp.ForceStepMapEdgeRepulsion = Gene[12] * OptimizedPropertiesMultiplyers::get().ForceStepMapEdgeRepulsionMultiplyer;
			opProp.ForceStepWeaponCoolDownRepulsion = Gene[13] * OptimizedPropertiesMultiplyers::get().ForceStepWeaponCoolDownRepulsionMultiplyer;

			//Range
			opProp.RangeOwnUnitsRepulsion = (int)floor(0.5 + Gene[14] * OptimizedPropertiesMultiplyers::get().RangeOwnUnitsRepulsionMultiplyer);
			opProp.RangePercentageSquadAttraction = (int)floor(0.5 + Gene[15] * OptimizedPropertiesMultiplyers::get().RangePercentageSquadAttractionMultiplyer);
			opProp.RangePecentageMapCenterAttraction = (int)floor(0.5 + Gene[16] * OptimizedPropertiesMultiplyers::get().RangePecentageMapCenterAttractionMultiplyer);
			opProp.RangeMapEdgeRepulsion = (int)floor(0.5 + Gene[17] * OptimizedPropertiesMultiplyers::get().RangeMapEdgeRepulsionMultiplyer);
			opProp.RangeWeaponCooldownRepulsion = (int)floor(0.5 + Gene[18] * OptimizedPropertiesMultiplyers::get().RangeWeaponCooldownRepulsionMultiplyer);

			return opProp;
		}
		logfile << "ERROR: In Method AddOptimizedValuesToUnitAgentChromosome in UnitAgentChromosome, the Gene was null or had none elements.\n";
		throw std::runtime_error("ERROR: In Method AddOptimizedValuesToUnitAgentChromosome in UnitAgentChromosome, the Gene was null or had none elements.");
	}

	void UnitAgentChromosome::InitRandomChromosome()
	{
		for (int i = 0; i < NumberOfUnitAgentOptimizedProperties; i++)
		{
			Gene.push_back(RandomNumber::get().RandomFloat(0, 2));
		}
	}
	// has to flip to put largest on top
	bool operator< (const UnitAgentChromosome& left, const UnitAgentChromosome& right)
	{
		return left.CompareTo(right) > 0;
	}
}