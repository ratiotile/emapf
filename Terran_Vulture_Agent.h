#pragma once
#include "UnitAgent.h"
#include "Interface.h"
#include <boost/shared_ptr.hpp>

class Terran_Vulture_Agent : public UnitAgent
{
public:
	Terran_Vulture_Agent(Unit* myUnit, UnitAgentOptimizedProperties opProp)
		: UnitAgent(myUnit, opProp)
	{
		UnitAgentTypeName = "Terran_Vulture_Agent";
		//SightRange = 8; 
	}
};
typedef boost::shared_ptr<Terran_Vulture_Agent> pTerran_Vulture_Agent;