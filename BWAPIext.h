#ifndef INCLUDED_BWAPIEXT
#define INCLUDED_BWAPIEXT
#ifndef INCLUDED_BWAPI_H
	#define INCLUDED_BWAPI_H
	#include <BWAPI.h>
#endif
#include "WalkTile.h"

using BWAPI::TilePosition;
using BWAPI::Position;
using BWAPI::UnitType;

// helper functions for BWAPI
namespace BWAPIext{
	/// take bounds of a rectangle and limit them to the size of the map
	void cropToMap( int& left, int& top, int& right, int& bottom, const int mapWidth, const int mapHeight );
	/// get px coordinates of bounding box around unit, radius offset and crop to map.
	void getBoundingBox( const BWAPI::Unit& rUnit, int& left, int& top, int& right, int& bottom, const int offset = 0, const bool crop = true );
	void getBoundingBox( const BWAPI::Unit* unit, Position& TL, Position&BR, 
		const unsigned int offset = 0, const bool crop = true);
	/// given a unit and radius, return a box around unit in buildtiles, cropped
	void getTileBox( const BWAPI::Unit* unit, unsigned int radius, TilePosition& TL, TilePosition& BR );
	/// given a bounding box in px, include all bt fully covered, covert to bt
	void getTileBounds( int& left, int& top, int& right, int& bottom );
	/// given a unit, provide bounding box in build tiles
	void getTileBounds( const BWAPI::Unit& rUnit,
		BWAPI::TilePosition& TL, BWAPI::TilePosition& BR);
	void getTileBounds( BWAPI::UnitType ut, BWAPI::TilePosition TL, BWAPI::TilePosition& BR);
	// Utility
	// convert TilePosition coordinates to pixel Positions; no checking is performed
	inline int BtoP(int tile) {return tile<<5;}
	// 32 pixels in a build tile
	inline int PtoB(int pixel) {return pixel>>5;}
	// 8 pixels in a walk tile
	inline int PtoW(int pixel) {return pixel>>3;}
	// 4 walk tiles in a build tile
	inline int BtoW(int tile) {return tile<<2;}
	/// convert walk tile coordinate to Build Tile
	inline int WtoB(int walk) {return walk>>2;}
	/// convert walktile coordinate to Position
	inline int WtoP(int walk) { return walk<<3;}
	// rounds down to nearest tile, positive only
	void floorTile( int& pixels );
	// rounds up to nearest tile, positive only
	void ceilTile( int& pixels );
	/// returns Position that is the center of the TilePosition
	BWAPI::Position getCenter(BWAPI::TilePosition tPos);
	/// returns first Terran Player in the game. Used for replays.
	BWAPI::Player* getTerranPlayer();
	/// returns string containing type and ID
	std::string getUnitNameID( const BWAPI::Unit* pUnit );
	/// returns string of coordinates
	std::string getString( const BWAPI::TilePosition& pos );
	/// calls BWAPI getUnitsOnTile
	inline std::set<BWAPI::Unit*>& getUnitsOnTile(BWAPI::TilePosition tp)
	{ return BWAPI::Broodwar->getUnitsOnTile(tp.x(),tp.y());	}
	/// return true if unit of type is in rectangle
	bool isUnitInArea( BWAPI::UnitType unitType, BWAPI::Position tl, BWAPI::Position br );
	/// gets string containing ID and player name
	std::string getPlayerNameID( const BWAPI::Player* pPlayer );
	/// filters out neutral units from  BWAPI::getUnitsInRadius()
	std::set<BWAPI::Unit*> getUnitsInRadius( BWAPI::Position center,
		int radius );
	/// return closest unit from set
	BWAPI::Unit* getClosestUnit( BWAPI::Position pos, std::set<BWAPI::Unit*>& unitGroup );
	/// true if unit has no attack
	bool isUnitNonCombat( const BWAPI::Unit* pUnit );
	/// units that can attack but not workers
	bool isCombatUnit( const BWAPI::UnitType ut);
	/// mobile controllable units
	bool isArmyUnit( const BWAPI::UnitType ut );
	/// true if unit is a combat unit producing building
	bool isFactory( const BWAPI::Unit* pUnit );
	bool isFactory( const BWAPI::UnitType unitType );
	bool isResourceDepot( const BWAPI::UnitType unitType );
	/// true if unit is gas refinery building
	bool isGasPlant( const BWAPI::UnitType unitType );
	/// false for Spider Mines and the like
	bool isControllable( BWAPI::UnitType unitType );
	/// true if unit hatches from Zerg_Egg
	bool isEggHatched( BWAPI::UnitType ut );
	/// true if unit is any kind of egg
	bool isEggType( BWAPI::UnitType ut );
	/// true if building morphed directly from Drone
	bool isDroneMorphed( BWAPI::UnitType ut );
	/// checks to see if tiles are visible for building placement
	bool isVisibleForBuild( BWAPI::TilePosition tl, BWAPI::UnitType toBuild );
	/// returns list of unfinished units of type, sorted most finished first
	std::list<BWAPI::Unit*> getUnfinishedUnits( BWAPI::UnitType unitType, 
		BWAPI::Player* player = BWAPI::Broodwar->self() );
	/// if unit is egg or incomplete unit of type specified
	bool isUnfinishedUnit( BWAPI::Unit* unit, BWAPI::UnitType ut );
	bool compare_buildTime( const BWAPI::Unit* first, const BWAPI::Unit* second );
	/// edge-to-edge distance between the two units, 0 if touching
	int getDistance(const BWAPI::Position &pos1, const BWAPI::UnitType type1, 
		const BWAPI::Position &pos2, const BWAPI::UnitType type2);
	int getDistance(const Position &pos1, const BWAPI::UnitType type1, const Position &pos2);
	/// is any part of where unit was visible?
	bool isAnyVisible(TilePosition location, UnitType type);
	bool isAllVisible(TilePosition location, BWAPI::UnitType type);
}
#endif

