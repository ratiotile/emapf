#pragma once
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include "Singleton.h"

using std::string;
// actually writes .info files
class IniFile: public Singleton<IniFile>
{
public:

	IniFile();

	template<typename Type>
	void write(const string& path, const Type&  value)
	{
		pt_.put<Type>(path,value);
	}

	template<typename Type>
	Type read(const string& path);

	void load();
	void save();
private:
	boost::property_tree::ptree pt_;
	string filepath;
};