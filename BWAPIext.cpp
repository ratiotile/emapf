#include "BWAPIext.h"
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace BWAPI;
using namespace BWAPI::UnitTypes;

void BWAPIext::cropToMap( int& left, int& top, int& right, int& bottom, const int mapWidth, const int mapHeight )
{
	if (left < 0) left = 0;
	if (top < 0) top = 0;
	if (right >= mapWidth) right = mapWidth - 1;
	if (bottom >= mapHeight) bottom = mapHeight - 1;
}

void BWAPIext::getBoundingBox( const Unit& rUnit, int& left, int& top, int& right, int& bottom, 
							  const int offset /*= 0*/, const bool crop /*= true */ )
{
	const int unitX = rUnit.getPosition().x();
	const int unitY = rUnit.getPosition().y();
	left = unitX - offset - rUnit.getType().dimensionLeft();
	right = unitX + offset + rUnit.getType().dimensionRight();
	top = unitY - offset - rUnit.getType().dimensionUp();
	bottom = unitY + offset + rUnit.getType().dimensionDown();
	if (crop)
	{
		cropToMap(left, top, right, bottom, Broodwar->mapWidth()*32, Broodwar->mapHeight()*32);
	}
}

void BWAPIext::getBoundingBox( const BWAPI::Unit* unit, Position& TL, Position&BR,
							  const unsigned int offset /*= 0*/, const bool crop /*= true */ )
{
	const Position unitP = unit->getPosition();
	const UnitType ut = unit->getType();
	TL = Position(unitP.x() - offset - ut.dimensionLeft(), unitP.y()-offset-ut.dimensionUp());
	BR = Position(unitP.x() + offset + ut.dimensionRight(), unitP.y()+offset+ut.dimensionDown());
	if (crop)
	{
		TL.makeValid();
		BR.makeValid();
	}
}
void BWAPIext::getTileBox( const BWAPI::Unit* unit, unsigned int radius, TilePosition& TL, TilePosition& BR )
{
	TilePosition unitTile = TilePosition(unit->getPosition());
	TL = TilePosition(unitTile.x()-radius,unitTile.y()-radius).makeValid();
	BR = TilePosition(unitTile.x()+radius,unitTile.y()+radius).makeValid();
}
void BWAPIext::getTileBounds( int& left, int& top, int& right, int& bottom )
{
	floorTile( left );
	floorTile( top );
	ceilTile( right );
	ceilTile( bottom );
}

void BWAPIext::getTileBounds( const BWAPI::Unit& rUnit, 
							 TilePosition& TL, TilePosition& BR )
{
	TL = rUnit.getTilePosition();
	BR = TilePosition(TL.x() + rUnit.getType().tileWidth() 
		, TL.y() + rUnit.getType().tileHeight() );
}

void BWAPIext::getTileBounds( BWAPI::UnitType ut, BWAPI::TilePosition TL, BWAPI::TilePosition& BR )
{
	BR = TilePosition(TL.x() + ut.tileWidth() 
		, TL.y() + ut.tileHeight() );
}
void BWAPIext::floorTile( int& pixels )
{
	pixels /= 32;
}

void BWAPIext::ceilTile( int& pixels )
{
	pixels = (( pixels + 31 ) / 32 );
}
/// returns NULL if no terran Player
Player* BWAPIext::getTerranPlayer()
{
	Player* terranPlayer = NULL;
	BOOST_FOREACH(Player* player, Broodwar->getPlayers())
	{
		if (player->getRace() == BWAPI::Races::Terran)
		{
			terranPlayer = player;
		}
	}
	return terranPlayer;
}

std::string BWAPIext::getUnitNameID( const BWAPI::Unit* pUnit )
{
	string id = boost::lexical_cast<string>( pUnit->getID() );
	string name;
	if (pUnit->getType() == Zerg_Egg)
	{
		name = pUnit->getType().getName() + "(" + pUnit->getBuildType().getName() + ") ";
	}
	else name = pUnit->getType().getName();
	string nameID = name + "[" + id + "]";
	return nameID;
}

std::string BWAPIext::getPlayerNameID( const BWAPI::Player* pPlayer )
{
	string id = boost::lexical_cast<string>( pPlayer->getID() );
	string nameID = pPlayer->getName() + "[" + id + "]";
	return nameID;
}

std::string BWAPIext::getString( const BWAPI::TilePosition& pos )
{
	string output = "[" + boost::lexical_cast<string>(pos.x())
		+ "," + boost::lexical_cast<string>(pos.y()) + "]";
	return output;
}

std::set<BWAPI::Unit*> BWAPIext::getUnitsInRadius( 
	BWAPI::Position center, int radius )
{
	set< BWAPI::Unit*> us = Broodwar->getUnitsInRadius(center, radius);
	for ( set< BWAPI::Unit*>::iterator i = us.begin(); i != us.end(); )
	{
		if ( (*i)->getPlayer()->isNeutral() )
		{
			set< BWAPI::Unit*>::iterator toDelete = i;
			++i;
			us.erase( toDelete );
		}
		else ++i;
	}
	return us;
}

bool BWAPIext::isUnitNonCombat( const BWAPI::Unit* pUnit )
{
	return !pUnit->getType().canAttack();
}

bool BWAPIext::isCombatUnit( const BWAPI::UnitType ut )
{
	if (!ut.canAttack() || ut.isWorker())
	{
		return false;
	}
	return true;
}

bool BWAPIext::isFactory( const BWAPI::Unit* pUnit )
{
	return isFactory( pUnit->getType() );
}

bool BWAPIext::isFactory( const BWAPI::UnitType unitType )
{
	if ( unitType == BWAPI::UnitTypes::Terran_Barracks 
		|| unitType == BWAPI::UnitTypes::Terran_Factory
		|| unitType == BWAPI::UnitTypes::Terran_Starport
		|| unitType == BWAPI::UnitTypes::Protoss_Gateway
		|| unitType == BWAPI::UnitTypes::Protoss_Stargate
		|| unitType == BWAPI::UnitTypes::Protoss_Robotics_Facility
		|| unitType == BWAPI::UnitTypes::Zerg_Hatchery
		|| unitType == BWAPI::UnitTypes::Zerg_Infested_Command_Center)
	{
		return true;
	}
	return false;
}
bool BWAPIext::isResourceDepot( const BWAPI::UnitType unitType )
{
	if ( unitType == BWAPI::UnitTypes::Protoss_Nexus
		|| unitType == BWAPI::UnitTypes::Terran_Command_Center
		|| unitType == BWAPI::UnitTypes::Zerg_Hatchery)
	{
		return true;
	}
	return false;
}

bool BWAPIext::isGasPlant( const BWAPI::UnitType unitType )
{
	if ( unitType == BWAPI::UnitTypes::Protoss_Assimilator 
		|| unitType == BWAPI::UnitTypes::Terran_Refinery
		|| unitType == BWAPI::UnitTypes::Zerg_Extractor )
	{
		return true;
	}
	return false;
}

bool BWAPIext::isControllable( BWAPI::UnitType unitType )
{
	if ( unitType == BWAPI::UnitTypes::Terran_Vulture_Spider_Mine
		|| unitType == BWAPI::UnitTypes::Terran_Nuclear_Missile
		|| unitType == BWAPI::UnitTypes::Protoss_Scarab
		|| unitType == BWAPI::UnitTypes::Protoss_Interceptor
		|| unitType.isSpell()
		)
	{
		return false;
	}
	return true;
}

BWAPI::Position BWAPIext::getCenter( BWAPI::TilePosition tPos )
{
	return Position(BtoP(tPos.x())+16, BtoP(tPos.y())+16);
}

bool BWAPIext::isVisibleForBuild( BWAPI::TilePosition tl, BWAPI::UnitType toBuild )
{
	int right = tl.x() + toBuild.tileWidth();
	int bottom = tl.y() + toBuild.tileHeight();
	BWAPI::TilePosition br(right,bottom);
	if (!tl.isValid() || !br.isValid())
	{
		return false;
	}
	for (int x = tl.x(); x < br.x(); x++)
	{
		for (int y = tl.y(); y < br.y(); y++)
		{
			if (!Broodwar->isVisible(BWAPI::TilePosition(x,y)))
			{
				return false;
			}
		}
	}
	return true;
}

bool BWAPIext::isUnitInArea( BWAPI::UnitType unitType, BWAPI::Position tl, BWAPI::Position br )
{
	// get set of all units in the rectangle
	set< BWAPI::Unit*> units = Broodwar->getUnitsInRectangle(tl,br);
	BOOST_FOREACH(  BWAPI::Unit* unit, units )
	{
		if (unit->getType() == unitType)
		{
			return true;
		}
	}
	return false;
}

BWAPI::Unit* BWAPIext::getClosestUnit( BWAPI::Position pos, set< BWAPI::Unit*>& unitGroup )
{
	 BWAPI::Unit* closest = NULL;
	int closestDist = numeric_limits<int>::max();
	BOOST_FOREACH( BWAPI::Unit* unit, unitGroup)
	{
		int dist = pos.getApproxDistance(unit->getPosition());
		if (closestDist > dist)
		{
			closest = unit;
			closestDist = dist;
		}
	}
	return closest;
}

bool BWAPIext::isEggHatched( BWAPI::UnitType ut )
{
	if ( ut.getRace() != BWAPI::Races::Zerg)
	{
		return false;
	}
	using namespace BWAPI::UnitTypes;
	if (	ut == Zerg_Drone ||
			ut == Zerg_Overlord ||
			ut == Zerg_Zergling ||
			ut == Zerg_Hydralisk ||
			ut == Zerg_Ultralisk ||
			ut == Zerg_Defiler ||
			ut == Zerg_Mutalisk ||
			ut == Zerg_Scourge ||
			ut == Zerg_Queen	
		)
	{
		return true;
	}
	return false;
}

bool BWAPIext::isEggType( BWAPI::UnitType ut )
{
	if ( ut == Zerg_Egg || ut == Zerg_Cocoon || ut == Zerg_Lurker_Egg)
	{
		return true;
	}
	return false;
}

bool BWAPIext::isDroneMorphed( BWAPI::UnitType ut )
{
	using namespace BWAPI::UnitTypes;
	if ( ut.getRace() != BWAPI::Races::Zerg)
	{
		return false;
	}
	if (	ut == Zerg_Hatchery ||
			ut == Zerg_Creep_Colony  ||
			ut == Zerg_Extractor ||
			ut == Zerg_Spawning_Pool ||
			ut == Zerg_Evolution_Chamber ||
			ut == Zerg_Spire ||
			ut == Zerg_Hydralisk_Den ||
			ut == Zerg_Queens_Nest ||
			ut == Zerg_Defiler_Mound ||
			ut == Zerg_Ultralisk_Cavern  ||
			ut == Zerg_Nydus_Canal
		)
	{
		return true;
	}
	return false;
}

std::list< BWAPI::Unit*> BWAPIext::getUnfinishedUnits( BWAPI::UnitType unitType, 
											  BWAPI::Player* player /*= Broodwar->self() */ )
{
	list< BWAPI::Unit*> unfinishedUnits;
	BOOST_FOREACH( BWAPI::Unit* unit, player->getUnits())
	{
		if (isUnfinishedUnit(unit,unitType))
		{
			unfinishedUnits.push_back(unit);
		}
	}
	// sort list and return
	unfinishedUnits.sort(compare_buildTime);
	return unfinishedUnits;
}

bool BWAPIext::isUnfinishedUnit( BWAPI::Unit* unit, BWAPI::UnitType ut )
{
	if (unit->getType() == ut && !unit->isCompleted()) return true;
	// check for zergs that turn into eggs/cocoons
	if (!unit->getType().isWorker() && unit->getBuildType() == ut)
	{
		return true;
	}
	return false;
}

bool BWAPIext::compare_buildTime( const BWAPI::Unit* first, const BWAPI::Unit* second )
{
	return first->getRemainingBuildTime() < second->getRemainingBuildTime();
}

int BWAPIext::getDistance( const BWAPI::Position &pos1, const BWAPI::UnitType type1, 
						   const BWAPI::Position &pos2, const BWAPI::UnitType type2 )
{
	const int uLeft       = pos1.x() - type1.dimensionLeft();
	const int uTop        = pos1.y() - type1.dimensionUp();
	const int uRight      = pos1.x() + type1.dimensionRight() + 1;
	const int uBottom     = pos1.y() + type1.dimensionDown() + 1;

	const int targLeft    = pos2.x() - type2.dimensionLeft();
	const int targTop     = pos2.y() - type2.dimensionUp();
	const int targRight   = pos2.x() + type2.dimensionRight() + 1;
	const int targBottom  = pos2.y() + type2.dimensionDown() + 1;

	int xDist = uLeft - targRight;
	if(xDist < 0)
	{
		xDist = targLeft - uRight;
		if(xDist < 0)
			xDist = 0;
	}

	int yDist = uTop - targBottom;
	if(yDist < 0)
	{
		yDist = targTop - uBottom;
		if(yDist < 0)
			yDist = 0;
	}

	return Position(0, 0).getApproxDistance(Position(xDist, yDist));
}

int BWAPIext::getDistance( const Position &pos1, const BWAPI::UnitType type1, const Position &pos2 )
{
	const int uLeft       = pos1.x() - type1.dimensionLeft();
	const int uTop        = pos1.y() - type1.dimensionUp();
	const int uRight      = pos1.x() + type1.dimensionRight() + 1;
	const int uBottom     = pos1.y() + type1.dimensionDown() + 1;

	int xDist = uLeft - (pos2.x() + 1);
	if(xDist < 0)
	{
		xDist = pos2.x() - uRight;
		if(xDist < 0)
			xDist = 0;
	}

	int yDist = uTop - (pos2.y() + 1);
	if(yDist < 0)
	{
		yDist = pos2.y() - uBottom;
		if(yDist < 0)
			yDist = 0;
	}

	return Position(0, 0).getApproxDistance(Position(xDist, yDist));
}
bool BWAPIext::isArmyUnit( const BWAPI::UnitType ut )
{
	if(ut.isBuilding())
		return false;
	if(ut.isSpell())
		return false;
	if(ut.isWorker())
		return false;
	if(ut == BWAPI::Broodwar->self()->getRace().getSupplyProvider())
		return false;
	if(ut == BWAPI::UnitTypes::Zerg_Egg)
		return false;
	if(ut == BWAPI::UnitTypes::Protoss_Interceptor)
		return false;
	if(ut == BWAPI::UnitTypes::Terran_Vulture_Spider_Mine)
		return false;
	if(ut == BWAPI::UnitTypes::Zerg_Larva)
		return false;
	if(ut == BWAPI::UnitTypes::Protoss_Scarab)
		return false;
	if(ut == BWAPI::UnitTypes::Protoss_Observer)
		return false;
	if(!ut.canAttack())
		return false;

	return true;
}

bool BWAPIext::isAnyVisible( TilePosition location, UnitType type )
{
	for(int x = location.x(); x < location.x() + type.tileWidth(); ++x)
	{
		for(int y = location.y(); y < location.y() + type.tileHeight(); ++y)
		{
			if(BWAPI::Broodwar->isVisible(x, y))
				return true;
		}
	}

	return false;
}

bool BWAPIext::isAllVisible( TilePosition location, BWAPI::UnitType type )
{
	for(int x = location.x(); x < location.x() + type.tileWidth(); ++x)
	{
		for(int y = location.y(); y < location.y() + type.tileHeight(); ++y)
		{
			if(!BWAPI::Broodwar->isVisible(x, y))
				return false;
		}
	}

	return true;
}

