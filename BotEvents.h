#pragma once
#include "Interface.h"
#include "Singleton.h"
#include "TacticalAssaultAgent.h"
#include <set>
#include <BWAPI.h>

class BotEvents: public Singleton<BotEvents>
{
	set<Unit*> _allUnits;
	vector<pUnitAgent> _myWorkers;
	vector<vector<pUnitAgent>> _myArmy; //All the squads will be in this list.
	//static List<UnitAgent> _mySquad;//The squad is a subset of myArmy, and represents a squad that is sent to attack the enemy.
	TacticalAssaultAgent _taa;
	bool _gameRestarted;

public:

	bool RanOutOfTime;
	vector<UnitAgentOptimizedProperties> OptimizedPropertiesGeneList;

	//UnitAgentOptimizedProperties OptimizedPropertiesGene { get; set; }

	void OnStart();
	bool OnSendText(string text);

	void OnReceiveText(Player* player, string text){}
	void OnPlayerLeft(Player* player){}
	void OnNukeDetect(Position target){}
	void OnUnitDiscover(Unit* unit){}
	void OnUnitEvade(Unit* unit){}

	/// Clean all the lists, so we secure that all units in the different lists 
	/// exists and none is added twice to the same list.
	void ResetUnitLists();
	pUnitAgent ConvertUnitToUnitAgent(Unit* unit);

	/// Get the optimized values to an unit agent. If there exists different 
	/// optimized values for the unit agents, these will be used, else just the 
	/// first UnitAgentOptimizedProperties will be used.
	UnitAgentOptimizedProperties GetOptimizedValuesToUnitAgent(string unitTypeName);

	/// Add all units to separate lists. For instance will there be a list with 
	/// own/my units and one for enemyUnits.
	void AddAllUnitsToSeparateLists();

	void OnFrame();

	void OnUnitCreate(Unit* unit){}

	/// Remove the destroyed unit from the list where the unit is placed.
	void RemoveDeadUnitFromList(Unit* unit);

	/// Checks if any of the (two) teams has any units left. 
	bool AnyUnitsLeftOnTeams();

	void OnUnitHide(Unit* unit);

	void OnUnitShow(Unit* unit){}

	void Reset();

	void OnEnd(bool isWinner);

	void OnUnitDestroy(Unit* unit);

	void OnUnitMorph(Unit* unit){}
	void OnUnitRenegade(Unit* unit){}
	void OnSaveGame(const string& gameName){}
};