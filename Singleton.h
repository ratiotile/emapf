/**
Singleton enforces the singleton design pattern
*/
#pragma once
#include <assert.h>

template<class T>
class Singleton{
public:
	/// Get Instance
	static T& get()
	{
		if (!pInstance)
		{
			pInstance = new T;
		}
		assert( pInstance );
		return *pInstance;
	}
protected:
	Singleton(){}
	~Singleton(){}
private:
	Singleton(Singleton const&);
	Singleton& operator=(Singleton const&);
	static T* pInstance;
};

template <class T> T* Singleton<T>::pInstance = 0;