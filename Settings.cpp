#include "Settings.h"
#include "IniFile.h"
#include "cpp_includes.h"

IniFile& ini = IniFile::get();

Settings::Settings():
	Debug(false),
	SerializeName("!StarCraftUnitAgent.txt"),
	LastChanceHandlerError("!LastChanceHandlerErrText.txt"),
	frameLimit(3200),
	totalNumberOfRepopulations(ini.read<unsigned int>("totalNumberOfRepopulations")),
	startGenerationOn(ini.read<unsigned int>("startGenerationOn")),
	percentOfOpPropsToLoad(ini.read<unsigned int>("percentOfOpPropsToLoad")),
	populationSize(ini.read<unsigned int>("populationSize")),
	numberOfTrials(ini.read<unsigned int>("numberOfTrials")),
	numberOfCrossovers(ini.read<unsigned int>("numberOfCrossovers")),
	gameSpeed(ini.read<int>("gameSpeed")),
	training(ini.read<bool>("training")),
	disableGUI(ini.read<bool>("disableGUI")),
	loadFileName(ini.read<string>("loadFromFile")),
	runOption(ini.read<string>("runOption")),
	numElites(ini.read<unsigned int>("numElites")),
	userSelectedControl(ini.read<bool>("userSelectedControl")),
	displaySelectedPF(ini.read<bool>("displaySelectedPF")),
	PFUpdateInterval(ini.read<unsigned int>("PFUpdateInterval"))
{
	BOOST_LOG_SEV(lg(),info) << "In constructor of Settings."
		<< "\ngameSpeed = " << gameSpeed 
		<< "\nPopulation Size = " << populationSize 
		<< "\nGenerations = " << totalNumberOfRepopulations+1
		<< "\nNumber of Trials = " << numberOfTrials
		<< "\nNumber of Crossovers = " << numberOfCrossovers
		<< "\nNumber of Elites = " << numElites
		<< "\nPFUpdateInterval = " << PFUpdateInterval;

	logfile << "Settings:\n" << "\nPopulation Size = " << populationSize 
		<< "\nGenerations = " << totalNumberOfRepopulations+1
		<< "\nNumber of Trials = " << numberOfTrials
		<< "\nNumber of Crossovers = " << numberOfCrossovers
		<< "\nNumber of Elites = " << numElites
		<< "\nPFUpdateInterval = " << PFUpdateInterval << "\n";
}