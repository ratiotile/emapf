#pragma once

#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"


class UnitAgentOptimizedProperties
{
public:
	/// these need to be saved somehow
	std::string UnitTypeName;
	int Fitness;
	int Rank;

	/// All the multiplyer properties that will be used to convert the chromosomes 
	/// to real Optimized properties
	double ForceOwnUnitsRepulsion;//= 100;//-500; // 0 - 1000
	double ForceEnemyUnitsRepulsion;//= 200; // 0 - 1000 //REMEMBER TO SET TO A POSITIVE NUMBER. 
	double ForceMSD;//= 240; //0-2000 or 1000  ....800
	double ForceSquadAttraction;//= 10; //0-1000
	double ForceMapCenterAttraction;//= 20; //0-1000
	double ForceMapEdgeRepulsion;//= 50;//0-1000
	double ForceWeaponCoolDownRepulsion;//= 800;//0-1000

	//Step is used for each distance step.
	double ForceStepOwnUnitsRepulsion;//= 0.6; // 0 - 10 SMALLER THAN punishmentRepulsionOwnUnits
	double ForceStepEnemyUnitsRepulsion;//= 1.2; // 0 - 10
	double ForceStepMSD;//= 0.24;//5; //0-10
	double ForceStepSquadAttraction;//= 0.1; //0 - 10
	double ForceStepMapCenterAttraction;// = 0.09;
	double ForceStepMapEdgeRepulsion;//= 0.3;
	double ForceStepWeaponCoolDownRepulsion;// = 6.4;

	int RangeOwnUnitsRepulsion;//= 8; //0-512    20
	int RangePercentageSquadAttraction;//= 5;//15; //0-100  //SHOULD SOMEHOW DEPEND ON HOW BIG THE SQUAD IS
	int RangePecentageMapCenterAttraction;// = 5; //0-100
	int RangeMapEdgeRepulsion;//= 96; //0-320
	int RangeWeaponCooldownRepulsion;//= 160;

	/// positive if this one greater, negative if this one less
	int CompareTo(const UnitAgentOptimizedProperties& other) const
	{
		return Fitness - other.Fitness;
	}

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & UnitTypeName;
		ar & Fitness;
		ar & Rank;

		ar & ForceOwnUnitsRepulsion; 
		ar & ForceEnemyUnitsRepulsion; 
		ar & ForceMSD; 
		ar & ForceSquadAttraction; 
		ar & ForceMapCenterAttraction; 
		ar & ForceMapEdgeRepulsion; 
		ar & ForceWeaponCoolDownRepulsion; 

		ar & ForceStepOwnUnitsRepulsion; 
		ar & ForceStepEnemyUnitsRepulsion; 
		ar & ForceStepMSD; 
		ar & ForceStepSquadAttraction; 
		ar & ForceStepMapCenterAttraction; 
		ar & ForceStepMapEdgeRepulsion; 
		ar & ForceStepWeaponCoolDownRepulsion; 

		ar & RangeOwnUnitsRepulsion; 
		ar & RangePercentageSquadAttraction; 
		ar & RangePecentageMapCenterAttraction; 
		ar & RangeMapEdgeRepulsion; 
		ar & RangeWeaponCooldownRepulsion; 
	}
};

bool operator< (const UnitAgentOptimizedProperties& left,
				const UnitAgentOptimizedProperties& right);